# Sharper Than Time

[![build status](https://gitlab.com/Grahf0085/sharper-than-time/badges/main/pipeline.svg)](https://gitlab.com/Grahf0085/sharper-than-time/-/jobs)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

A simple app to read and interact with several books written by Nietzsche.

## Features

- Bookmark where you left off in a book
- Make and save notes for each paragraph. Edit at a later time if needed
- Search individual books
- Search multiple books
- View alternative translations of a paragraph you're reading
- Customize the style of the book, font, color, etc, to make it more readable for you
- Resizing the browser window should keep your place in the book
- View footnotes inside each paragraph
- View paragraphs from other books that might be of interested to the paragraph you're currently reading

## Screenshots

<table>
  <tr>
    <td>
      <figure>
        <figcaption>Dark Theme</figcaption>
        <img
          src="./public/screenshots/dark-theme.png"
          alt="Dark Theme"
          width="400"
        />
      </figure>
    </td>
    <td>
      <figure>
        <figcaption>Light Theme</figcaption>
        <img
          src="./public/screenshots/light-theme.png"
          alt="Light Theme"
          width="400"
        />
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure>
        <figcaption>Search Current Book</figcaption>
        <img
          src="./public/screenshots/single-book-search.png"
          alt="Search Current Book"
          width="400"
        />
      </figure>
    </td>
    <td>
      <figure>
        <figcaption>Search Multiple Books</figcaption>
        <img
          src="./public/screenshots/multi-book-search.png"
          alt="Search Multiple Books"
          width="400"
        />
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure>
        <figcaption>
          View Another Translation Of What You're Currently Reading
        </figcaption>
        <img
          src="./public/screenshots/alt-translation.png"
          alt="View Another Translation"
          width="400"
        />
      </figure>
    </td>
    <td>
      <figure>
        <figcaption>View Footnotes Inside Of Paragraphs</figcaption>
        <img
          src="./public/screenshots/footnotes.png"
          alt="View Footnotes"
          width="400"
        />
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure>
        <figcaption>Create Notes And Save Them</figcaption>
        <img
          src="./public/screenshots/create-notes.png"
          alt="Create Notes"
          width="400"
        />
      </figure>
    </td>
    <td>
      <figure>
        <figcaption>View All Of Your Notes</figcaption>
        <img
          src="./public/screenshots/view-notes.png"
          alt="View Your Notes"
          width="400"
        />
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure>
        <figcaption>Bookmark Where You Leave Off</figcaption>
        <img
          src="./public/screenshots/create-bookmark.png"
          alt="Create A Bookmark"
          width="400"
        />
      </figure>
    </td>
    <td>
      <figure>
        <figcaption>Return To Your Bookmark Whenever You Want</figcaption>
        <img
          src="./public/screenshots/go-to-bookmark.png"
          alt="Go To Bookmark"
          width="400"
        />
      </figure>
    </td>
  </tr>
  <tr>
    <td>
      <figure>
        <figcaption>Change Style Of The Text To Suit You</figcaption>
        <img
          src="./public/screenshots/font.png"
          alt="Change Font"
          width="400"
        />
      </figure>
    </td>
  </tr>
</table>

## Source Material

Most of the text from the books are from [Project Gutenburg](https://www.gutenberg.org). The books included in the app are from the [Public Domain](https://en.wikipedia.org/wiki/Public_domain)

## Running Locally

1. Create the database using the [backend repo](https://gitlab.com/Grahf0085/make-nietzsche-db)
2. Clone this repo
3. Run `npm install`
4. Run `npm run dev`

## Bugs or Suggestions

To report a bug or make a suggestion please search the [issues tab](https://gitlab.com/Grahf0085/sharper-than-time/-/issues) and see if your issue is already logged, or create a new one. Or you can e-mail me at <sharperthantime@tutanota.com>

## License

Sharper Than Time is licensed under [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)

## This app works better in firefox than in chromium or a chromium based browser.

## Acknowledgment

Thanks to everyone who has translated the books and solidJS people

<link rel="stylesheet" type="text/css" href="./readmestyles.css">
