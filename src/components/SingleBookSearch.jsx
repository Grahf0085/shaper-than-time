import { createEffect, Show } from 'solid-js'
import { FormError, useParams } from 'solid-start'
import { createServerAction$ } from 'solid-start/server'
import { TbSearch } from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import {
  createSearchResults,
  createSetSearchResults,
} from '~/providers/SearchProvider'
import { getSingleBookSearch } from '~/server/books'

function validateSearchTerm(searchTerm) {
  if (typeof searchTerm !== 'string' || searchTerm.length < 3)
    return `Search term must be at least three characters long`
}

export const SingleBookSearch = () => {
  const params = useParams()

  const setSearchResults = createSetSearchResults()
  const searchResults = createSearchResults()

  const [searching, { Form }] = createServerAction$(async (form) => {
    const searchString = form.get('search')
    const title = form.get('title')
    const translator = form.get('translator')

    if (
      typeof searchString !== 'string' ||
      typeof title !== 'string' ||
      typeof translator !== 'string'
    )
      throw new FormError(`Form not submitted correctly.`)

    const fields = { searchString }

    const fieldErrors = {
      searchString: validateSearchTerm(searchString),
    }

    if (Object.values(fieldErrors).some(Boolean)) {
      throw new FormError('', { fieldErrors, fields })
    }

    const fetchedResults = await getSingleBookSearch(
      searchString,
      title,
      translator,
    )

    if (fetchedResults.length === 0) {
      throw new FormError(`I don't know if N said that here`, { fields })
    }

    return { searchString, fetchedResults }
  })

  createEffect(() => {
    if (searching.result)
      setSearchResults({
        term: searching?.result?.searchString,
        matches: searching?.result?.fetchedResults,
      })
  })

  createEffect(() => {
    if (searching.error) {
      setTimeout(() => {
        searching.clear()
      }, 3500)
    }
  })

  return (
    <Form class='relative h-7 w-full max-lg:mb-1 max-sm:my-1 md:order-first md:w-min md:flex-auto lg:order-last lg:mx-1 lg:w-full lg:max-w-xs'>
      <div class='flex items-center'>
        <input name='title' type='hidden' value={params.title} />
        <input name='translator' type='hidden' value={params.translator} />
        <input
          value={searchResults()?.term || ''}
          name='search'
          placeholder='Search Selected Book'
          class='w-full rounded-sm border-2 border-subMenuColor bg-transparent pl-2 outline-none placeholder:italic placeholder:text-textColor placeholder:opacity-50'
        />
        <Show when={searching.pending}>
          <ImSpinner2 size={26} class='animate-spin' />
        </Show>
        <Show when={searching.error?.fieldErrors?.searchString}>
          <p class='absolute -top-32 whitespace-normal rounded-sm bg-subMenuColor px-4 py-1 md:-top-10 md:whitespace-nowrap lg:-top-16 lg:whitespace-normal'>
            {searching.error.fieldErrors.searchString}
          </p>
        </Show>
        <Show when={searching.error}>
          <p class='absolute -top-[6.5rem] rounded-sm bg-subMenuColor px-4 py-1 md:-top-10 lg:-top-10'>
            {searching.error.message}
          </p>
        </Show>
        <button
          typ='submit'
          disabled={searching.pending}
          class='rounded-sm bg-subMenuColor px-4 py-1'
        >
          <TbSearch size={20} />
        </button>
      </div>
    </Form>
  )
}
