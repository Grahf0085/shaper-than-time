import { Show } from 'solid-js'
import { Portal } from 'solid-js/web'
import { TbX } from 'solid-icons/tb'

export const SeeAlso = (props) => {
  return (
    <>
      <button
        onClick={props.onSeeAlsoClick}
        type='button'
        class={`${
          props.otherTranslator ? 'bg-menuColor' : 'bg-subMenuColor'
        } rounded-sm p-4 lg:m-1 my-1 w-fit`}
      >
        <cite>{props.seeAlso.bookTitle}</cite>({props.seeAlso.translatorName}
        ), Chapter {props.seeAlso.chapterNumber}, Paragraph{' '}
        {props.seeAlso.paragraphNumber}
      </button>
      <Show when={props.isActive}>
        <Portal mount={props.seeAlsoRef}>
          <figure
            class={`w-full h-max rounded-sm p-4 lg:ml-1 ${
              props.otherTranslator ? 'bg-menuColor' : 'bg-subMenuColor'
            }`}
          >
            <button
              type='button'
              onClick={props.onSeeAlsoClick}
              class={`rounded-sm p-2 ${
                props.otherTranslator ? 'bg-subMenuColor' : 'bg-menuColor'
              }`}
            >
              <TbX size={30} />
            </button>
            <figcaption class='py-2'>
              <cite>
                {props.seeAlso.bookTitle}({props.seeAlso.translatorName})
              </cite>{' '}
              Chapter {props.seeAlso.chapterNumber}
            </figcaption>
            <blockquote>
              <p
                class={`${
                  props.otherTranslator ? 'bg-menuColor' : 'bg-subMenuColor'
                }`}
              >
                {props.seeAlso.paragraphText}
              </p>
            </blockquote>
          </figure>
        </Portal>
      </Show>
    </>
  )
}
