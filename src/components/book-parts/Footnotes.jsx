import { createMarker } from '@solid-primitives/marker'
import { Footnote } from './Footnote'

export const Footnotes = (props) => {
  let count = -1

  const createFootnote = createMarker((footnote) => {
    count++
    return (
      <Footnote
        footnoteWord={footnote()}
        otherTranslator={props.otherTranslator}
        count={count}
        chapterNumber={props.chapterNumber}
        paragraphNumber={props.paragraphNumber}
      />
    )
  })

  return (
    <>
      {createFootnote(
        props.text,
        /(“|")[^(”|")]+(”|")[⁰¹²³⁴⁵⁶⁷⁸⁹]+|[⁰¹²³⁴⁵⁶⁷⁸⁹]+|\S+[⁰¹²³⁴⁵⁶⁷⁸⁹]+/gm,
      )}
    </>
  )
}
