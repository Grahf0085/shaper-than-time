import { For, onCleanup } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { createSetBookRefs } from '~/providers/RefProvider'
import { getChapterList } from '~/server/books'
import { Paragraphs } from './Paragraphs'

export const Chapters = (props) => {
  const params = useParams()

  const setBookRefs = createSetBookRefs()

  const chapterList = createServerData$(
    (value) => getChapterList(value[0], value[1]),
    {
      key() {
        return [params.title, params.translator]
      },
    },
  )

  return (
    <For each={chapterList()} fallback={<div>Loading Chapters...</div>}>
      {(chapter) => (
        <>
          <h3
            ref={(el) => {
              setBookRefs((p) => [...p, el])
              props.setChapterRefs((p) => [...p, el])
              onCleanup(() => {
                props.setChapterRefs((p) => p.filter((i) => i !== el))
              })
            }}
            class='w-full snap-center font-bold'
            data-chapter={`Chapter: ${chapter.chapterNumber}`}
          >
            {chapter.chapterName}
          </h3>
          <Paragraphs chapterNumber={chapter.chapterNumber} />
          <div class='h-full' />
        </>
      )}
    </For>
  )
}
