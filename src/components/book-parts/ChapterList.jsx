import { For } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import {
  createSelectedFont,
  createSelectedFontSize,
  createSelectedLineHeight,
} from '~/providers/FontProvider'
import { createSetBookRefs } from '~/providers/RefProvider'
import { getChapterList } from '~/server/books'

export const ChapterList = (props) => {
  const params = useParams()

  const font = createSelectedFont()
  const lineHeight = createSelectedLineHeight()
  const fontSize = createSelectedFontSize()
  const setBookRefs = createSetBookRefs()

  const chapterList = createServerData$(
    (value) => getChapterList(value[0], value[1]),
    {
      key() {
        return [params.title, params.translator]
      },
    },
  )

  const handleChapterLink = (chapterNumber) =>
    props.chapterRefs[chapterNumber].scrollIntoView({ behavior: 'smooth' })

  return (
    <div
      class={`w-full h-full snap-center overflow-y-scroll flex justify-center ${
        props.sideMenu === true ? 'mb-0' : 'mb-4'
      }`}
    >
      <ul
        ref={(el) => {
          setBookRefs((p) => [...p, el])
        }}
        class={`w-full h-full flex ${
          props.sideMenu === true ? 'flex-row md:flex-col' : 'flex-col'
        }`}
        data-chapter='Contents'
        data-book={params.title + params.translator}
      >
        <For each={chapterList()} fallback={<div>Loading Chapter List...</div>}>
          {(chapter) => (
            <li
              class={`w-full hover:bg-subMenuColor ${
                props.sideMenu === true
                  ? 'bg-subMenuColor md:bg-menuColor'
                  : 'mb-4 bg-subMenuColor'
              } rounded-sm ${
                parseInt(
                  props.currentChapter && props.currentChapter.split(':')[1],
                ) !== chapter.chapterNumber
                  ? 'border-none'
                  : 'border-solid border-textColor max-md:border-b-2 md:border-l-2'
              }`}
            >
              <button
                type='button'
                class={`text-center md:text-left h-full p-4 ${
                  props.sideMenu === true
                    ? 'w-14 min-w-full md:w-full'
                    : 'w-full'
                }`}
                style={{
                  'font-family': props.sideMenu === true ? 'rubik' : font(),
                  'font-size':
                    props.sideMenu === true ? '0.875rem' : fontSize(),
                  'line-height':
                    props.sideMenu === true ? '1.25rem' : lineHeight(),
                }}
                onClick={() => handleChapterLink(chapter.chapterNumber)}
              >
                <h3
                  class={`${
                    props.sideMenu === true ? 'max-md:hidden' : 'block'
                  }`}
                >
                  {chapter.chapterName}
                </h3>
                <h3
                  class={`${
                    props.sideMenu === true ? 'block md:hidden' : 'hidden'
                  }`}
                >
                  {chapter.chapterNumber}
                </h3>
              </button>
            </li>
          )}
        </For>
      </ul>
    </div>
  )
}
