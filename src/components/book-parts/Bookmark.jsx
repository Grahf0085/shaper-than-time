import { createEffect, createSignal, Show } from 'solid-js'
import { FormError, useParams } from 'solid-start'
import { createServerAction$, createServerData$ } from 'solid-start/server'
import { TbBookmark } from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import { createBookmark } from '~/server/reader'
import { getReader } from '~/server/session'

export const Bookmark = (props) => {
  const [confirmedBookmarkMessage, setConfirmedBookmarkMessage] = createSignal()

  const params = useParams()

  const reader = createServerData$((_, { request }) => getReader(request))

  const [makingBookmark, { Form }] = createServerAction$(async (form) => {
    const readerId = form.get('readerId')
    const paragraphId = form.get('paragraphId')

    if (typeof readerId !== 'string' || typeof paragraphId !== 'string')
      throw new FormError(`Form not submitted correctly.`)

    const fetchedBookmark = await createBookmark(readerId, paragraphId)

    if (!fetchedBookmark) {
      throw new FormError('Bookmark failed :(')
    }

    return fetchedBookmark
  })

  createEffect(() => {
    if (makingBookmark.result) {
      if (window.matchMedia('(max-width: 768px)').matches) {
        setConfirmedBookmarkMessage('Bookmarked!')
      } else {
        setConfirmedBookmarkMessage(
          `Bookmarked ${decodeURI(params.title)}, chapter ${
            props.chapterNumber
          }, paragraph ${props.paragraphNumber}, translated by ${decodeURI(
            params.translator,
          )}`,
        )
      }
      setTimeout(() => {
        makingBookmark.clear()
      }, 3000)
    }
  })

  return (
    <div class='contents'>
      <Show when={reader()}>
        <Form class='flex items-center justify-center'>
          <input value={props.paragraphId} type='hidden' name='paragraphId' />
          <input value={reader().id} type='hidden' name='readerId' />
          <button
            type='submit'
            disabled={makingBookmark.pending}
            class='rounded-sm p-2 hover:bg-subMenuColor'
          >
            <TbBookmark size={26} />
          </button>
        </Form>
        <Show when={makingBookmark.pending}>
          <ImSpinner2
            size={26}
            class='col-start-4 row-start-1 animate-spin self-center'
          />
        </Show>
        <Show when={makingBookmark.result}>
          <p class='col-start-4 row-start-1 self-center'>
            {confirmedBookmarkMessage()}
          </p>
        </Show>
        <Show when={makingBookmark.error}>
          <p class='col-start-4 row-start-1 self-center'>
            {makingBookmark.error.message}
          </p>
        </Show>
      </Show>
    </div>
  )
}
