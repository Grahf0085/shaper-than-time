import { createEffect, createSignal, Show } from 'solid-js'
import { createServerAction$, createServerData$ } from 'solid-start/server'
import { FormError } from 'solid-start/data'
import { TbWriting, TbPencilPlus, TbPencilOff } from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import { getReader } from '~/server/session'
import { createNotes, getNotes, updateNotes } from '~/server/reader'
import { Transition } from 'solid-transition-group'

function validateNotes(notes) {
  if (typeof notes !== 'string' || notes.length < 3 || notes.length > 3000)
    return `Notes must be between three and 3000 characters long`
}

export const ReaderNote = (props) => {
  const [showNotes, setShowNotes] = createSignal(false)

  const reader = createServerData$((_, { request }) => getReader(request))

  const [fetchedNotes, fetchNotes] = createServerAction$(async (args) => {
    return await getNotes(args[0], args[1])
  })

  const [makingNotes, { Form }] = createServerAction$(async (form) => {
    const readerId = form.get('readerId')
    const paragraphId = form.get('paragraphId')
    const savedNotes = form.get('savedNotes')
    const notes = form.get('notes')

    if (
      typeof notes !== 'string' ||
      typeof readerId !== 'string' ||
      typeof paragraphId !== 'string' ||
      typeof savedNotes !== 'string'
    )
      throw new FormError('Form not submitted correctly.')

    const fields = { notes }

    const fieldErrors = {
      notes: validateNotes(notes),
    }

    if (Object.values(fieldErrors).some(Boolean)) {
      throw new FormError('', { fieldErrors, fields })
    }

    let newNote

    if (savedNotes === '')
      newNote = await createNotes(readerId, paragraphId, notes)
    else newNote = await updateNotes(readerId, paragraphId, notes)

    if (!newNote) {
      throw new FormError('Note failed :(')
    }

    return newNote
  })

  createEffect(() => {
    if (makingNotes.result) {
      setTimeout(() => {
        makingNotes.clear()
      }, 3000)
    }
  })

  return (
    <div class='contents'>
      <Show when={reader()}>
        <button
          onClick={() => {
            setShowNotes(!showNotes())
            if (showNotes()) {
              fetchNotes([reader()?.id, props.paragraphId])
            }
          }}
          type='button'
          class='flex justify-center rounded-sm p-2 hover:bg-subMenuColor'
        >
          <Show when={!showNotes()} fallback={<TbPencilOff size={26} />}>
            <TbWriting size={26} />
          </Show>
        </button>
      </Show>
      <Transition
        onEnter={(el, done) => {
          const a = el.animate([{ opacity: 0 }, { opacity: 1 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
        onExit={(el, done) => {
          const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
      >
        <Show when={showNotes()}>
          <Form class='col-start-1 col-end-5 row-start-3 mt-2 w-full'>
            <input value={props.paragraphId} type='hidden' name='paragraphId' />
            <input value={reader().id} type='hidden' name='readerId' />
            <input
              value={fetchedNotes.result || ''}
              type='hidden'
              name='savedNotes'
            />
            <label>
              {reader()?.username}'s Thoughts
              <textarea
                value={fetchedNotes.result || ''}
                name='notes'
                disabled={fetchedNotes.pending}
                class='w-full rounded-sm border-2 border-subMenuColor bg-transparent outline-none focus:border-2 focus:border-subMenuColor focus:ring-0'
              />
            </label>
            <Show when={makingNotes.error?.fieldErrors?.notes}>
              <p>{makingNotes.error.fieldErrors.notes}</p>
            </Show>
            <button
              type='submit'
              disabled={makingNotes.pending || fetchedNotes.pending}
              class='flex items-center'
            >
              <TbPencilPlus size={26} />
              Save
            </button>
          </Form>
        </Show>
      </Transition>
      <Show when={makingNotes.pending || fetchedNotes.pending}>
        <ImSpinner2
          size={26}
          class='col-start-4 row-start-1 animate-spin self-center'
        />
      </Show>
      <Show when={makingNotes.result}>
        <p class='col-start-4 row-start-1 self-center'>Notes Saved!</p>
      </Show>
      <Show when={makingNotes.error}>
        <p class='col-start-4 row-start-1 self-center'>
          {makingNotes.error.message}
        </p>
      </Show>
    </div>
  )
}
