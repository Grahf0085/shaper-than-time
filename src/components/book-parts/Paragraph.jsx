import { createEffect, createSignal, createMemo, Show, For } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { createMediaQuery } from '@solid-primitives/media'
import { createMarker, makeSearchRegex } from '@solid-primitives/marker'
import { Footnotes } from './Footnotes'
import { SeeAlso } from './SeeAlso'
import {
  createParagraphRefs,
  createSetBookRefs,
  createSetParagraphRefs,
} from '~/providers/RefProvider'
import { getSeeAlso } from '~/server/books'

export const Paragraph = (props) => {
  const [activeSeeAlso, setActiveSeeAlso] = createSignal(null)
  const [seeAlsoRef, setSeeAlsoRef] = createSignal()

  const params = useParams()

  const setBookRefs = createSetBookRefs()
  const setParagraphRefs = createSetParagraphRefs()
  const paragraphRefs = createParagraphRefs()

  const seeAlsos = createServerData$((value) => getSeeAlso(value[0]), {
    key() {
      return [props.paragraphId]
    },
  })

  const isLarge = createMediaQuery('(min-width: 1024px)')

  const highlight = createMarker((text) => <mark>{text()}</mark>)
  const regex = createMemo(() => makeSearchRegex(props.highlight || ''))

  const handleShowSeeAlso = (index) => {
    setActiveSeeAlso((p) => (p === index ? null : index))
    if (seeAlsoRef() !== null)
      seeAlsoRef().scrollIntoView({ behavior: 'smooth' })
  }

  return (
    <div class='flex max-lg:flex-col'>
      <p
        ref={(el) => {
          //we only make refs if the paragraphs are main in the main window - refs not made for search results
          if (props.makeRefs === true) {
            setBookRefs((p) => [...p, el])

            const newRef = { ref: el, id: () => props.paragraphId }
            const existingIndex = paragraphRefs().findIndex(
              (item) => item.id() === newRef.id(),
            )
            if (existingIndex !== -1) {
              paragraphRefs()[existingIndex].ref = newRef.ref
            } else {
              setParagraphRefs((p) => [...p, newRef])
            }

            createEffect(() => {
              setParagraphRefs((p) =>
                p.filter(
                  (i) =>
                    i.ref.getAttribute('data-book') ===
                    params.title + params.translator,
                ),
              )
            })
          }

          createEffect(() => {
            const book = params.title + params.translator
            if (book) setActiveSeeAlso(null)
            return book
          })
        }}
        //transition duration wont work with tailwind for some reason so using style here
        style={{
          width: activeSeeAlso() !== null && isLarge() ? '66.666667%' : '100%',
          'transition-duration': '500ms',
        }}
        class='w-fit flex-nowrap whitespace-pre-wrap'
        data-chapter={`Chapter: ${props.chapterNumber}`}
        data-book={params.title + params.translator}
      >
        <Show when={props.highlight}>
          <p>{highlight(props.text, regex())}</p>
        </Show>
        <Show when={props.text.matchAll(/[⁰¹²³⁴⁵⁶⁷⁸⁹]+/gm)}>
          {/* footnotes actually makes the paragraph text too !... */}
          <Footnotes
            chapterNumber={props.chapterNumber}
            paragraphNumber={props.paragraphNumber}
            text={props.text}
            otherTranslator={props.otherTranslator}
          />
        </Show>
        <span class='my-2 flex h-fit w-full max-lg:flex-col lg:items-center'>
          <Show when={seeAlsos()?.length > 0} fallback={<></>}>
            <span class='font-semibold'>See Also:</span>
          </Show>
          <For each={seeAlsos()} fallback={<></>}>
            {(seeAlso, index) => (
              <SeeAlso
                seeAlso={seeAlso}
                otherTranslator={props.otherTranslator}
                isActive={activeSeeAlso() === index()}
                onSeeAlsoClick={[handleShowSeeAlso, index()]}
                seeAlsoRef={seeAlsoRef()}
              />
            )}
          </For>
        </span>
      </p>
      <div
        ref={(el) => setSeeAlsoRef(el)}
        class={`
         ${
           !isLarge()
             ? 'w-fit'
             : activeSeeAlso() !== null && isLarge()
             ? 'w-1/3'
             : 'w-0'
         } duration-500`}
      />
    </div>
  )
}
