import { createEffect, For, Show } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import {
  createSelectedFont,
  createSelectedFontSize,
  createSelectedLineHeight,
} from '~/providers/FontProvider'
import { createSetBookRefs } from '~/providers/RefProvider'
import { getBookInfo } from '~/server/books'

export const BookInfo = () => {
  const params = useParams()

  const font = createSelectedFont()
  const lineHeight = createSelectedLineHeight()
  const fontSize = createSelectedFontSize()
  const setBookRefs = createSetBookRefs()

  const titleSize = () => parseFloat(fontSize()) * 2 + 'rem'
  const otherInfoFontSize = () => parseFloat(fontSize()) * 1.5 + 'rem'

  const bookInfo = createServerData$(
    (value) => getBookInfo(value[0], value[1]),
    {
      key() {
        return [params.title, params.translator]
      },
    },
  )

  return (
    <For each={bookInfo()} fallback={<div>Loading Book Info...</div>}>
      {(info) => (
        <div
          ref={(el) => {
            setBookRefs((p) => [...p, el])
            createEffect(() => {
              setBookRefs((p) =>
                p.filter(
                  (i) =>
                    i.getAttribute('data-book') ===
                    params.title + params.translator,
                ),
              )
            })
          }}
          style={{
            'line-height': lineHeight(),
            'font-family': font(),
          }}
          class='flex h-full w-full snap-center flex-col md:justify-around overflow-y-scroll'
          data-chapter='Title'
          data-book={params.title + params.translator}
        >
          <div class='flex flex-col items-center justify-evenly gap-10 md:justify-center'>
            <h1
              style={{
                'font-size': titleSize(),
              }}
              class='text-center font-bold'
            >
              {info.bookTitle}
              {info.subTitle !== '' ? ': ' + info.subTitle : ''}
            </h1>
            <h2
              style={{
                'font-size': otherInfoFontSize(),
              }}
              class='text-center font-medium'
            >
              Published: <time datetime={info.pubDate}>{info.pubDate}</time>
            </h2>
            <h2
              style={{
                'font-size': otherInfoFontSize(),
              }}
              class='text-center font-medium'
            >
              Translated By {info.translatorName},{' '}
              <time datetime={info.translatedDate}>{info.translatedDate}</time>
            </h2>
            <Show
              when={info.translatorName !== 'Ian Johnston'}
              fallback={
                <p class='self-center text-sm'>
                  Special thanks to Ian Johnston for his translation
                  (http://fs2.american.edu/dfagel/www/genealogy1.htm)
                </p>
              }
            >
              <p class='text-sm lg:w-3/5'>
                This eBook is for the use of anyone anywhere in the United
                States and most other parts of the world at no cost and with
                almost no restrictions whatsoever. You may copy it, give it away
                or re-use it under the terms of the Project Gutenberg License
                included with this eBook or online at www.gutenberg.org. If you
                are not located in the United States, you’ll have to check the
                laws of the country where you are located before using this
                ebook.
              </p>
            </Show>
          </div>
        </div>
      )}
    </For>
  )
}
