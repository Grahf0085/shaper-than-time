import { createEffect, createSignal, Show } from 'solid-js'
import { useParams } from 'solid-start'
import { TbLanguage, TbLanguageOff } from 'solid-icons/tb'
import { Paragraph } from './Paragraph'
import { Transition } from 'solid-transition-group'

export const OtherTranslations = (props) => {
  const [showTranslation, setShowTranslation] = createSignal(false)

  const params = useParams()

  createEffect(() => {
    const book = params.title + params.translator
    if (book) setShowTranslation(false)
  })

  return (
    <div class='contents'>
      <button
        onClick={() => setShowTranslation(!showTranslation())}
        type='button'
        class='flex justify-center rounded-sm p-2 hover:bg-subMenuColor'
      >
        <Show when={!showTranslation()} fallback={<TbLanguageOff size={26} />}>
          <TbLanguage size={26} />
        </Show>
      </button>
      <Transition
        onEnter={(el, done) => {
          const a = el.animate([{ opacity: 0 }, { opacity: 1 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
        onExit={(el, done) => {
          const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
      >
        <Show when={showTranslation()}>
          <aside class='col-start-1 col-end-5 row-start-2 mt-2 rounded-sm bg-subMenuColor p-4'>
            <h2 class='font-bold'>{props.translator} Translation</h2>
            <Paragraph
              chapterNumber={props.chapterNumber}
              paragraphNumber={props.paragraphNumber}
              text={props.text}
              paragraphId={props.paragraphId}
              makeRefs={false}
              otherTranslator={props.translator}
            />
          </aside>
        </Show>
      </Transition>
    </div>
  )
}
