import { For, Suspense, Show } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { Paragraph } from './Paragraph'
import { OtherTranslations } from './OtherTranslations'
import { Bookmark } from './Bookmark'
import { ReaderNote } from './ReaderNote'
import {
  getChapterParagraphs,
  getOtherTranslationOfParagraph,
} from '~/server/books'

export const Paragraphs = (props) => {
  const params = useParams()

  const paragraphs = createServerData$(
    (value) => getChapterParagraphs(value[0], value[1], value[2]),
    {
      key() {
        return [params.title, params.translator, props.chapterNumber]
      },
    },
  )

  const otherTranslations = createServerData$(
    (value) => getOtherTranslationOfParagraph(value[0], value[1], value[2]),
    {
      key() {
        return [params.title, params.translator, props.chapterNumber]
      },
    },
  )

  return (
    <For each={paragraphs()} fallback={<div>Loading Paragraphs...</div>}>
      {(paragraph, index) => (
        //w-full needed for a few paragraphs in chapter four of BGE. Otherwise they are wider than the screen. Unsure why.
        <div class='mb-4 w-full snap-center overflow-y-auto md:mb-6'>
          <Paragraph
            chapterNumber={props.chapterNumber}
            paragraphNumber={paragraph.paragraphNumber}
            text={paragraph.paragraphText}
            paragraphId={paragraph.paragraphId}
            makeRefs={true}
          />
          <div class='grid grid-cols-[3rem,3rem,3rem,auto] grid-rows-[auto,auto,auto]'>
            <Bookmark
              paragraphId={paragraph.paragraphId}
              paragraphNumber={paragraph.paragraphNumber}
              chapterNumber={props.chapterNumber}
            />
            <ReaderNote paragraphId={paragraph.paragraphId} />
            <Suspense>
              <Show when={otherTranslations()?.length > index()}>
                <OtherTranslations
                  chapterNumber={props.chapterNumber}
                  paragraphNumber={otherTranslations()[index()].paragraphNumber}
                  text={otherTranslations()[index()].paragraphText}
                  paragraphId={otherTranslations()[index()].paragraphId}
                  translator={otherTranslations()[index()].translatorName}
                />
              </Show>
            </Suspense>
          </div>
        </div>
      )}
    </For>
  )
}
