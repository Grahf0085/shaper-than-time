import { createSignal, Show } from 'solid-js'
import { useParams } from 'solid-start'
import { createServerAction$ } from 'solid-start/server'
import { Transition } from 'solid-transition-group'
import { ImSpinner2 } from 'solid-icons/im'
import { getFootnotes } from '~/server/books'

export const Footnote = (props) => {
  const [showFootnotes, setShowFootnotes] = createSignal(false)

  const params = useParams()

  const [fetchedFootnotes, fetchFootnotes] = createServerAction$(
    async (args) => {
      return await getFootnotes(args[0], args[1], args[2], args[3])
    },
  )

  return (
    <span class='relative'>
      <button
        type='button'
        onClick={() => {
          setShowFootnotes(!showFootnotes())
          if (showFootnotes()) {
            fetchFootnotes([
              params.title,
              props.otherTranslator || params.translator,
              props.chapterNumber,
              props.paragraphNumber,
            ])
          }
        }}
        class='text-left font-bold'
      >
        {props.footnoteWord + ' '}
      </button>
      <Transition
        onEnter={(el, done) => {
          const a = el.animate([{ opacity: 0 }, { opacity: 1 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
        onExit={(el, done) => {
          const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
            duration: 500,
          })
          a.finished.then(done)
        }}
      >
        <Show when={fetchedFootnotes.pending}>
          <ImSpinner2
            size={26}
            class='animate-spin absolute left-0 right-0 top-0 bottom-0 m-auto'
          />
        </Show>
        <Show
          when={
            showFootnotes() === true &&
            !fetchedFootnotes.pending &&
            fetchedFootnotes.result
          }
        >
          <aside
            class={`${
              props.otherTranslator ? 'bg-menuColor' : 'bg-subMenuColor'
            } rounded-sm inline-flex w-fit font-bold`}
          >
            {fetchedFootnotes.result[props.count] + ' '}
          </aside>
        </Show>
      </Transition>
    </span>
  )
}
