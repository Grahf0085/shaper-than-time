import { SingleBookSearch } from './SingleBookSearch'
import { createFullTextRef } from '~/providers/RefProvider'

export const Slider = (props) => {
  let sliderRef

  const fullTextRef = createFullTextRef()

  const handleInput = () => {
    const percentScrolled = sliderRef.value / props.maxPage
    const scrollWidth = fullTextRef().scrollWidth - fullTextRef().clientWidth
    fullTextRef().scrollLeft = percentScrolled * scrollWidth
    fullTextRef().focus()
  }

  return (
    <div class='flex w-full flex-wrap items-center justify-between self-center from-menuColor via-subMenuColor to-menuColor font-rubik text-base lg:flex-row lg:flex-nowrap lg:bg-gradient-to-r'>
      <div class='h-7 w-32 whitespace-nowrap rounded-sm bg-subMenuColor px-4 py-1 text-center max-lg:mb-1 md:mx-1'>
        {props.currentChapter === 'Chapter: 0'
          ? 'Preface'
          : `${props.currentChapter}`}
      </div>
      <div class='h-7 w-32 whitespace-nowrap rounded-sm bg-subMenuColor px-4 py-1 text-center max-lg:mb-1 lg:mx-1'>
        {props.currentPage + 1}/{props.maxPage + 1}
      </div>
      <input
        type='range'
        value={props.currentPage}
        max={props.maxPage}
        onInput={handleInput}
        onChange={(event) => props.setCurrentPage(parseInt(event.target.value))}
        ref={sliderRef}
        class='slider h-7 w-full cursor-pointer appearance-none overflow-hidden rounded-sm border-2 border-subMenuColor bg-subMenuColor lg:mx-1'
      />
      <SingleBookSearch />
    </div>
  )
}
