import { Show, For, createSignal, startTransition } from 'solid-js'
import { A } from '@solidjs/router'
import { useParams, useIsRouting } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import {
  TbLayoutColumns,
  TbBook,
  TbBookOff,
  TbChevronUp,
  TbChevronDown,
  TbX,
} from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import face from '~/assesets/face.png'
import {
  createDrawerOpen,
  createSetDrawerOpen,
} from '~/providers/OtherProvider'
import {
  createSetSearchResults,
  createSelectedTitles,
  createSetSelectedTitles,
} from '~/providers/SearchProvider'
import { getAllTitles, getTranslations } from '~/server/books'

export const Header = () => {
  const [mobileMenuOpen, setMobileMenuOpen] = createSignal(false)
  const [selectedTitle, setSelectedTitle] = createSignal()

  const params = useParams()
  const isRouting = useIsRouting()

  const drawerOpen = createDrawerOpen()
  const setDrawerOpen = createSetDrawerOpen()
  const setSelectedTitles = createSetSelectedTitles()
  const selectedTitles = createSelectedTitles()
  const setSearchResults = createSetSearchResults()

  const titles = createServerData$(() => getAllTitles())

  const translators = createServerData$((value) => getTranslations(value), {
    key() {
      return selectedTitle()
    },
  })

  return (
    <header class='flex h-20 justify-between font-rubik'>
      <Show when={isRouting()}>
        <ImSpinner2
          size={200}
          class='animate-spin absolute left-0 right-0 top-0 bottom-0 m-auto'
        />
      </Show>
      <button
        onClick={() => {
          setDrawerOpen(!drawerOpen())
          setMobileMenuOpen(false)
        }}
        class='flex h-full w-24 items-center justify-center rounded-sm duration-500 hover:bg-subMenuColor'
      >
        <Show when={drawerOpen()} fallback={<TbLayoutColumns size={36} />}>
          <TbX size={36} />
        </Show>
      </button>
      <nav class='flex w-full rounded-sm bg-menuBackground bg-right bg-no-repeat lg:justify-around'>
        {/* =========================================================================== */}
        {/* MOBILE BOOK MENU */}
        {/* =========================================================================== */}

        <button
          onClick={() => {
            setMobileMenuOpen(!mobileMenuOpen())
            setSelectedTitle(undefined)
            setDrawerOpen(false)
          }}
          class='lg:hidden'
        >
          <Show
            when={mobileMenuOpen() === false}
            fallback={<TbBookOff size={36} />}
          >
            <TbBook size={36} />
          </Show>
        </button>
        {/* =========================================================================== */}
        {/* END MOBILE BOOK MENU */}
        {/* =========================================================================== */}

        <div
          class={`${
            mobileMenuOpen() ? 'left-0' : '-left-[100%]'
          } lg:flex lg:static fixed top-16 w-full lg:justify-around duration-500 bg-menuColor lg:bg-transparent h-full z-50`}
        >
          <For each={titles()} fallback={<div>Loading Titles...</div>}>
            {(title) => (
              <ul
                onMouseOver={() =>
                  startTransition(() => setSelectedTitle(title))
                }
                class='group lg:relative'
              >
                <li class='rounded-sm p-4 text-xl group-hover:bg-menuColor'>
                  <button
                    onTouchEnd={() =>
                      startTransition(() =>
                        selectedTitle() === title
                          ? setSelectedTitle(undefined)
                          : setSelectedTitle(title),
                      )
                    }
                    class={`${
                      decodeURI(params.title) === title
                        ? 'border-solid'
                        : 'border-dashed'
                    } duration-500 p-2 border-textColor group-hover:border-subMenuColor lg:cursor-default flex justify-between w-full lg:border-b-2 relative`}
                  >
                    {title}
                    <Show
                      when={selectedTitle() === title}
                      fallback={
                        <span class='lg:hidden'>
                          <TbChevronDown size={30} />
                        </span>
                      }
                    >
                      <span class='lg:hidden'>
                        <TbChevronUp size={30} />
                      </span>
                    </Show>
                  </button>
                </li>
                <ul
                  class={`lg:absolute lg:block lg:top-16 w-full lg:opacity-0 bg-subMenuColor group-hover:opacity-100 duration-500 rounded-sm ${
                    selectedTitle() === title ? 'max-lg:block' : 'max-lg:hidden'
                  }`}
                >
                  <For each={translators()}>
                    {(translator) => (
                      <A href={`/book/${selectedTitle()}/${translator}`}>
                        <button
                          onClick={() => {
                            setMobileMenuOpen(false)
                            setSearchResults({ term: '', matches: [] })
                          }}
                          class='w-full bg-menuColor lg:hidden lg:group-hover:block'
                        >
                          <li class='mb-6 rounded-sm bg-subMenuColor p-4 text-left duration-500 hover:bg-ternaryColor lg:m-0 lg:text-center'>
                            {translator}
                          </li>
                        </button>
                      </A>
                    )}
                  </For>
                </ul>
              </ul>
            )}
          </For>
        </div>
      </nav>
      <A href='/'>
        <button
          type='button'
          onClick={() => {
            setSelectedTitle(undefined)
            setMobileMenuOpen(false)
            setSearchResults({ term: '', matches: [] })
            setSelectedTitles(selectedTitles().map(() => false))
          }}
          class='flex h-full w-24 items-center justify-center rounded-sm duration-500 hover:bg-subMenuColor'
        >
          <img
            src={face}
            alt="Nietzsche's Likeness"
            class='w-6/12 rounded-full'
          />
        </button>
      </A>
    </header>
  )
}
