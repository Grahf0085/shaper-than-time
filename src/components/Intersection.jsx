import { onCleanup, onMount } from 'solid-js'
import { createBookRefs } from '~/providers/RefProvider'
import { createSetTextOnScreen } from '~/providers/OtherProvider'

export const Intersection = (props) => {
  let intersectionObserver

  const bookRefs = createBookRefs()
  const setTextOnScreen = createSetTextOnScreen()

  const intersectionObserverOptions = {
    root: null, // relative to document viewport
    rootMargin: '0px', // margin around root. Values are similar to css property. Unitless values not allowed
    threshold: 0.1, // visible amount of item shown in relation to root
  }

  const intersectionObserverCallback = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        props.setCurrentChapter(entry.target.getAttribute('data-chapter'))
        setTimeout(() => {
          setTextOnScreen(entry.target)
        }, 1000)
      }
    })
  }

  onMount(() => {
    intersectionObserver = new IntersectionObserver(
      intersectionObserverCallback,
      intersectionObserverOptions,
    )
    props.setIntersectionObserver(intersectionObserver)
    bookRefs().forEach((reference) => intersectionObserver.observe(reference))
  })

  onCleanup(() => {
    if (intersectionObserver) intersectionObserver.disconnect()
  })
}
