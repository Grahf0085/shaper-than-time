import { For, Show } from 'solid-js'
import { useIsRouting, useNavigate, useParams } from 'solid-start'
import { TbX } from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import { Paragraph } from './book-parts/Paragraph'
import { createSetDrawerOpen } from '~/providers/OtherProvider'
import {
  createSearchResults,
  createSetSearchResults,
} from '~/providers/SearchProvider'
import { createParagraphRefs } from '~/providers/RefProvider'

export const SearchResults = () => {
  const navigate = useNavigate()
  const params = useParams()
  const isRouting = useIsRouting()

  const searchResults = createSearchResults()
  const paragraphRefs = createParagraphRefs()
  const setSearchResults = createSetSearchResults()
  const setDrawerOpen = createSetDrawerOpen()

  const scrollToSelectedSearch = (para) => {
    if (para.bookTitle || para.translatorName) {
      navigate(`/book/${para.bookTitle}/${para.translatorName}`)

      const checkIfDoneRouting = setInterval(function () {
        const decodedTitle = decodeURI(params.title)
        const decodedTranslator = decodeURI(params.translator)
        const searchResultBookTitle = para.bookTitle
        const searchResultTranslatorName = para.translatorName

        if (
          decodedTitle === searchResultBookTitle &&
          decodedTranslator === searchResultTranslatorName
        ) {
          const matchingRef = () =>
            paragraphRefs().find((ref) => ref.id() === para.paragraphId)

          if (window.matchMedia('(min-width: 1024px)').matches) {
            matchingRef()?.ref.scrollIntoView({
              behavior: 'smooth',
            })
          }

          if (window.matchMedia('(max-width: 1024px)').matches) {
            setSearchResults({ term: '', matches: [] })
            setDrawerOpen(false)
            setTimeout(() => {
              matchingRef()?.ref.scrollIntoView({
                behavior: 'smooth',
              })
            }, 1000)
          }

          clearInterval(checkIfDoneRouting)
        }
      }, 1000)
    } else {
      const matchingRef = () =>
        paragraphRefs().find((ref) => ref.id() === para.paragraphId)

      if (window.matchMedia('(min-width: 1024px)').matches) {
        matchingRef()?.ref.scrollIntoView({
          behavior: 'smooth',
        })
      }

      if (window.matchMedia('(max-width: 1024px)').matches) {
        setSearchResults({ term: '', matches: [] })
        setDrawerOpen(false)
        setTimeout(() => {
          matchingRef()?.ref.scrollIntoView({
            behavior: 'smooth',
          })
        }, 1000)
      }
    }
  }

  return (
    <>
      <button
        type='button'
        class='relative ml-4 mr-auto rounded-sm bg-subMenuColor p-2'
        onClick={() => {
          //timeout needed for animation purposes when sidebar closes - so there's still something to animate
          setTimeout(() => {
            setSearchResults({ term: '', matches: [] })
          }, 0)
        }}
      >
        <TbX size={30} />
      </button>
      <ol>
        <For each={searchResults().matches}>
          {(para, index) => (
            <li>
              <button
                type='button'
                onClick={() => scrollToSelectedSearch(para)}
                class='rounded-sm p-4 text-start hover:bg-subMenuColor'
              >
                <span class='font-bold'>{index() + 1} - </span>
                <Show
                  when={para.bookTitle}
                  fallback={
                    <span>
                      {para.chapterName}, Paragraph {para.paragraphNumber}
                    </span>
                  }
                >
                  <cite>{para.bookTitle}</cite>({para.translatorName}),
                  <div>
                    {para.chapterName}, Paragraph {para.paragraphNumber}
                  </div>
                </Show>
                <Paragraph
                  chapterNumber={para.chapterNumber}
                  paragraphNumber={para.paragraphNumber}
                  //slice and join used to cut out partial words at beginning and end of paragraphText
                  //PROBLEM - if first or last word is word that was searched for is it being cut off?
                  text={para.paragraphText.split(' ').slice(1, -1).join(' ')}
                  paragraphId={para.paragraphId}
                  makeRefs={false}
                  highlight={searchResults().term}
                />
              </button>
            </li>
          )}
        </For>
      </ol>
      <Show when={isRouting()}>
        <ImSpinner2
          size={200}
          class='fixed bottom-0 left-0 right-0 top-0 m-auto animate-spin self-center lg:hidden'
        />
      </Show>
    </>
  )
}
