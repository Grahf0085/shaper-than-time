import { createEffect, For, Suspense, Show } from 'solid-js'
import { FormError } from 'solid-start'
import { createServerAction$, createServerData$ } from 'solid-start/server'
import { TbSearch } from 'solid-icons/tb'
import { ImSpinner2 } from 'solid-icons/im'
import {
  createSetSearchResults,
  createSetSelectedTitles,
  createSelectedTitles,
} from '~/providers/SearchProvider'
import { getAllTitles, getMultipleBookSearch } from '~/server/books'

function validateSearchTerm(searchTerm) {
  if (typeof searchTerm !== 'string' || searchTerm.length < 3)
    return `Search term must be at least three characters long`
}

function validateSelectedBook(selectedBooks) {
  if (!Array.isArray(selectedBooks) || selectedBooks.length < 1)
    return `You must select a book to search`
}

export const SearchAllBooks = () => {
  const setSearchResults = createSetSearchResults()
  const setSelectedTitles = createSetSelectedTitles()
  const selectedTitles = createSelectedTitles()

  const titles = createServerData$(() => getAllTitles())

  const [searching, { Form }] = createServerAction$(async (form) => {
    const searchString = form.get('search')
    const entries = form.entries()
    const selectedBooks = []

    for (const [name, value] of entries) {
      if (name === 'book-select') selectedBooks.push(value)
    }

    if (typeof searchString !== 'string' || !Array.isArray(selectedBooks))
      throw new FormError(`Form not submitted correctly.`)

    const fields = { searchString, selectedBooks }

    const fieldErrors = {
      searchString: validateSearchTerm(searchString),
      selectedBooks: validateSelectedBook(selectedBooks),
    }

    if (Object.values(fieldErrors).some(Boolean)) {
      throw new FormError('', { fieldErrors, fields })
    }

    const fetchedResults = await getMultipleBookSearch(
      searchString,
      selectedBooks,
    )

    if (fetchedResults.length === 0) {
      throw new FormError(`I don't know if N ever said that`, { fields })
    }

    return { searchString, fetchedResults }
  })

  createEffect(() => {
    if (searching.result)
      setSearchResults({
        term: searching?.result?.searchString,
        matches: searching?.result?.fetchedResults,
      })
  })

  return (
    <Suspense>
      <Form class='grid grid-cols-1 grid-rows-[min-content,min-content] max-md:my-4 max-md:h-min max-md:bg-subMenuColor'>
        <fieldset class='flex max-md:mx-4 max-md:flex-wrap max-md:gap-4 max-md:bg-subMenuColor md:flex-col'>
          <legend class='text-sm max-md:ml-1 max-md:mt-1 md:text-xs'>
            select books to search
          </legend>
          <For each={titles()} fallback={<div>Loading Titles...</div>}>
            {(title, index) => {
              setSelectedTitles(() => titles().map(() => false))
              return (
                <label
                  class={`rounded-sm hover:bg-subMenuColor self-center md:h-full md:w-full md:p-4 px-4 py-2 cursor-pointer md:my-1 ${
                    selectedTitles()[index()] === true
                      ? 'border-solid border-textColor max-md:border-x-2 max-md:border-b-2 max-md:border-t-2 max-md:border-x-transparent max-md:border-t-transparent md:border-l-2'
                      : 'border-textColor max-md:border-2 max-md:border-dashed md:border-x-[1px] md:border-dashed'
                  }`}
                >
                  {title}
                  <input
                    type='checkbox'
                    id={title}
                    name='book-select'
                    value={title}
                    checked={selectedTitles()[index()]}
                    onChange={() => {
                      const nextSelectedTitles = selectedTitles().map(
                        (input, i) => {
                          if (i === index()) return !input
                          else return input
                        },
                      )
                      setSelectedTitles(nextSelectedTitles)
                    }}
                    class='hidden'
                  />
                </label>
              )
            }}
          </For>
        </fieldset>
        <div class='flex h-min flex-col max-lg:mx-3 max-lg:mt-4'>
          <div class='flex'>
            <button
              typ='submit'
              disabled={searching.pending}
              class='mt-1 rounded-sm bg-menuColor px-4 py-1 md:bg-subMenuColor'
            >
              <TbSearch size={20} />
            </button>
            <input
              name='search'
              placeholder='Search For...'
              class='mt-1 h-7 w-full rounded-sm border-2 border-menuColor bg-transparent pl-2 outline-none placeholder:italic placeholder:text-textColor placeholder:opacity-50 md:border-subMenuColor'
            />
            <Show when={searching.pending}>
              <ImSpinner2 size={26} class='animate-spin self-center' />
            </Show>
          </div>
          <Show when={searching.error?.fieldErrors?.searchString}>
            <p class='mt-1'>{searching.error.fieldErrors.searchString}</p>
          </Show>
          <Show when={searching.error?.fieldErrors?.selectedBooks}>
            <p class='mt-1'>{searching.error.fieldErrors.selectedBooks}</p>
          </Show>
          <Show when={searching.error}>
            <p class='mt-1'>{searching.error.message}</p>
          </Show>
        </div>
      </Form>
    </Suspense>
  )
}
