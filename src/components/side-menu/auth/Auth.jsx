import { createSignal, Show } from 'solid-js'
import { useLocation } from 'solid-start'
import { createServerAction$ } from 'solid-start/server'
import { FormError } from 'solid-start/data'
import { createReaderSession, login, register } from '~/server/session'
import { findReader } from '~/server/reader'

function validateUsername(username) {
  if (typeof username !== 'string' || username.length < 3)
    return `Usernames must be at least three characters long`
}

function validatePassword(password) {
  if (typeof password !== 'string' || password.length < 6)
    return `Passwords must be at least six characters long`
}

export const Auth = () => {
  const [formType, setFormType] = createSignal('login')

  const location = useLocation()

  const [loggingIn, { Form }] = createServerAction$(async (form) => {
    const loginType = form.get('loginType')
    const username = form.get('username')
    const password = form.get('password')
    const redirectTo = form.get('redirectTo') || '/'

    if (
      typeof loginType !== 'string' ||
      typeof username !== 'string' ||
      typeof password !== 'string' ||
      typeof redirectTo !== 'string'
    )
      throw new FormError(`Form not submitted correctly.`)

    const fields = { loginType, username, password }

    const fieldErrors = {
      username: validateUsername(username),
      password: validatePassword(password),
    }

    if (Object.values(fieldErrors).some(Boolean)) {
      throw new FormError('', { fieldErrors, fields })
    }

    switch (loginType) {
      case 'login': {
        const user = await login({ username, password })
        if (!user) {
          throw new FormError(`Username/Password combination is incorrect`, {
            fields,
          })
        }
        return createReaderSession(`${user.id}`, redirectTo)
      }
      case 'register': {
        const userExists = await findReader({ where: { username } })
        if (userExists) {
          throw new FormError(`User with username ${username} already exists`, {
            fields,
          })
        }
        const user = await register({ username, password })
        if (!user) {
          throw new FormError(
            `Something went wrong trying to create a new user.`,
            {
              fields,
            },
          )
        }
        return createReaderSession(`${user.id}`, redirectTo)
      }
      default: {
        throw new FormError(`Login type invalid`, { fields })
      }
    }
  })

  return (
    <Form class='max-lg:w-full max-md:mx-2'>
      <input type='hidden' name='redirectTo' value={location.pathname ?? '/'} />
      <fieldset class='max-lg:mb-4 max-lg:flex max-lg:text-center'>
        <label
          class={`block rounded-sm hover:bg-subMenuColor p-4 cursor-pointer max-lg:w-1/2 ${
            formType() === 'login'
              ? 'border-solid border-textColor max-lg:border-b-2 lg:border-l-2'
              : 'border-none'
          }`}
        >
          Login
          <input
            type='radio'
            name='loginType'
            value='login'
            checked={formType() === 'login'}
            onChange={() => setFormType('login')}
            class='hidden'
          />
        </label>
        <label
          class={`block rounded-sm hover:bg-subMenuColor p-4 cursor-pointer max-lg:w-1/2 ${
            formType() === 'register'
              ? 'border-solid border-textColor max-lg:border-b-2 lg:border-l-2'
              : 'border-none'
          }`}
        >
          Register
          <input
            type='radio'
            name='loginType'
            value='register'
            checked={formType() === 'register'}
            onChange={() => setFormType('register')}
            class='hidden'
          />
        </label>
      </fieldset>
      <fieldset>
        <div class='w-full'>
          <input
            name='username'
            placeholder='Enter a Handle'
            class='h-7 w-full rounded-sm border-2 border-menuColor bg-transparent pl-2 outline-none placeholder:italic placeholder:text-textColor placeholder:opacity-50 md:border-subMenuColor'
          />
          <Show when={loggingIn.error?.fieldErrors?.username}>
            <p>{loggingIn.error.fieldErrors.username}</p>
          </Show>
          <input
            name='password'
            type='password'
            placeholder='Enter a Password'
            //no sure why this input needs a p-0 and gets default padding
            class='mt-1 h-7 w-full rounded-sm border-2 border-menuColor bg-transparent p-0 pl-2 text-sm outline-none placeholder:italic placeholder:text-textColor placeholder:opacity-50 focus:border-2 focus:!border-menuColor focus:outline-none focus:ring-0 md:border-subMenuColor md:focus:!border-subMenuColor'
          />
          <Show when={loggingIn.error?.fieldErrors?.password}>
            <p class='mt-1'>{loggingIn.error.fieldErrors.password}</p>
          </Show>
          <Show when={loggingIn.error}>
            <p class='mt-1'>{loggingIn.error.message}</p>
          </Show>
        </div>
      </fieldset>
      <button
        type='submit'
        class='my-1 w-full rounded-sm bg-subMenuColor px-4 py-1'
      >
        {formType().charAt(0).toUpperCase() + formType().substring(1)}
      </button>
    </Form>
  )
}
