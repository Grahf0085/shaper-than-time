import { useLocation } from 'solid-start'
import { createServerAction$ } from 'solid-start/server'
import { TbLogout } from 'solid-icons/tb'
import { logout } from '~/server/session'

export const Logout = () => {
  const location = useLocation()

  const [, { Form }] = createServerAction$((form, { request }) => {
    const currentLocation = form.get('currentLocation') || '/'

    if (currentLocation.startsWith('/reader')) return logout(request, '/')
    else return logout(request, currentLocation)
  })

  return (
    <Form class='w-full'>
      <input
        type='hidden'
        name='currentLocation'
        value={location.pathname ?? '/'}
      />
      <button
        name='logout'
        type='submit'
        class='rounded-sm p-4 hover:bg-subMenuColor lg:w-full'
      >
        <figure class='flex'>
          <TbLogout size={36} />
          <figcaption class='flex items-center max-md:hidden'>
            Logout
          </figcaption>
        </figure>
      </button>
    </Form>
  )
}
