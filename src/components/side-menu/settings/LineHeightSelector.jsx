import { For } from 'solid-js'
import {
  createSelectedLineHeight,
  createSetSelectedLineHeight,
} from '~/providers/FontProvider'

export const LineHeightSelector = () => {
  const lineHeights = [
    { name: 'Tight', height: 1.25 },
    { name: 'Snug', height: 1.375 },
    { name: 'Normal', height: 1.5 },
    { name: 'Relaxed', height: 1.625 },
    { name: 'Loose', height: 2 },
  ]

  const selectedLineHeight = createSelectedLineHeight()
  const setSelectedLineHeight = createSetSelectedLineHeight()

  return (
    <For each={lineHeights} fallback={<div>Loading Line Heights</div>}>
      {(lineHeight) => {
        return (
          <li
            class={`font-rubik rounded-sm hover:bg-subMenuColor self-center ${
              selectedLineHeight() === lineHeight.height
                ? 'border-solid md:border-l-2 border-textColor max-md:border-b-2'
                : 'border-none'
            }`}
          >
            <button
              type='button'
              onClick={() => setSelectedLineHeight(lineHeight.height)}
              class='w-full h-full text-start p-4'
            >
              {lineHeight.name}
            </button>
          </li>
        )
      }}
    </For>
  )
}
