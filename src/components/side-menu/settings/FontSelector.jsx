import { For } from 'solid-js'
import {
  createSelectedFont,
  createSetSelectedFont,
} from '~/providers/FontProvider.jsx'

export const FontSelector = () => {
  const fonts = [
    'Atkinson Hyperlegible',
    'Bookerly',
    'Inter',
    'Lexend',
    'Merriweather',
  ]

  const selectedFont = createSelectedFont()
  const setSelectedFont = createSetSelectedFont()

  return (
    <For each={fonts} fallback={<div>Loading Fonts</div>}>
      {(font) => {
        return (
          <li
            style={{
              'font-family': font,
            }}
            class={`rounded-sm hover:bg-subMenuColor self-center ${
              selectedFont() === font
                ? 'border-solid md:border-l-2 border-textColor max-md:border-b-2'
                : 'border-none'
            }`}
          >
            <button
              type='button'
              onClick={() => setSelectedFont(font)}
              class='w-full h-full text-start p-4 whitespace-nowrap'
            >
              {font}
            </button>
          </li>
        )
      }}
    </For>
  )
}
