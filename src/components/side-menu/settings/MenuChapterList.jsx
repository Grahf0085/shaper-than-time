import { createSetSideBarRef } from '~/providers/RefProvider'

export const MenuChapterList = () => {
  const setSideBarRef = createSetSideBarRef()

  return <div class='w-full' ref={(el) => setSideBarRef(el)} />
}
