import { For } from 'solid-js'
import {
  createSelectedFontSize,
  createSetSelectedFontSize,
} from '~/providers/FontProvider'

export const FontSizeSelector = () => {
  const fontSizes = [
    { name: 'small', size: '0.875rem' },
    { name: 'normal', size: '1rem' },
    { name: 'large', size: '1.125rem' },
    { name: 'x-large', size: '1.25rem' },
  ]

  const selectedFontSize = createSelectedFontSize()
  const setSelectedFontSize = createSetSelectedFontSize()

  return (
    <For each={fontSizes} fallback={<div>Loading Font Sizes</div>}>
      {(fontSize) => {
        return (
          <li
            class={`font-rubik rounded-sm hover:bg-subMenuColor self-center ${
              selectedFontSize() === fontSize.size
                ? 'border-solid md:border-l-2 border-textColor max-md:border-b-2'
                : 'border-none'
            }`}
          >
            <button
              type='button'
              onClick={() => setSelectedFontSize(fontSize.size)}
              class='w-max md:w-full h-full text-start p-4'
            >
              {fontSize.name}
            </button>
          </li>
        )
      }}
    </For>
  )
}
