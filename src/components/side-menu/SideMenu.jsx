import { Show, createEffect, createSignal } from 'solid-js'
import { A } from '@solidjs/router'
import { useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import {
  TbTypography,
  TbTextResize,
  TbLineHeight,
  TbListNumbers,
  TbSearch,
  TbLogin,
  TbNotes,
} from 'solid-icons/tb'
import { Theme } from './settings/Theme'
import { SideMenuItem } from './SideMenuItem'
import { FontSelector } from './settings/FontSelector'
import { FontSizeSelector } from './settings/FontSizeSelector'
import { LineHeightSelector } from './settings/LineHeightSelector'
import { MenuChapterList } from './settings/MenuChapterList'
import { SearchAllBooks } from './SearchMultipleBooks'
import { Auth } from './auth/Auth'
import { Logout } from './auth/Logout'
import { GoToBookmark } from './GoToBookmark'
import { createDrawerOpen } from '~/providers/OtherProvider'
import { getReader } from '~/server/session'

export const SideMenu = () => {
  const [fontsOpen, setFontsOpen] = createSignal(false)
  const [fontSizeOpen, setFontSizeOpen] = createSignal(false)
  const [lineHeightOpen, setLineHeightOpen] = createSignal(false)
  const [chaptersOpen, setChaptersOpen] = createSignal(false)
  const [searchOpen, setSearchOpen] = createSignal(false)
  const [authOpen, setAuthOpen] = createSignal(false)

  const setSignals = [
    setFontsOpen,
    setFontSizeOpen,
    setLineHeightOpen,
    setChaptersOpen,
    setSearchOpen,
    setAuthOpen,
  ]

  const params = useParams()

  const reader = createServerData$((_, { request }) => getReader(request))

  const drawerOpen = createDrawerOpen()

  createEffect(() => {
    if (!drawerOpen()) {
      setSignals.forEach((signal) => signal(false))
    }
  })

  return (
    <div class='flex h-fit w-full items-center overflow-x-scroll font-rubik max-md:relative md:flex-col lg:h-full'>
      <Theme />
      <Show
        when={!reader()}
        fallback={
          <>
            <A
              href={`/reader/${reader().username}`}
              class='rounded-sm p-4 hover:bg-subMenuColor md:w-full'
            >
              <figure class='flex items-center'>
                <TbNotes size={36} />
                <figcaption class='text-sm max-md:hidden'>
                  {reader().username}'s Notes
                </figcaption>
              </figure>
            </A>
            <GoToBookmark />
            <Logout />
          </>
        }
      >
        <SideMenuItem
          setSignals={setSignals}
          ownSetter={setAuthOpen}
          ownGetter={authOpen()}
          icon={<TbLogin size={36} />}
          title={'Login/Register'}
          component={<Auth />}
        />
      </Show>
      <Show when={params.title} fallback={<div />}>
        <SideMenuItem
          setSignals={setSignals}
          ownSetter={setFontsOpen}
          ownGetter={fontsOpen()}
          icon={<TbTypography size={36} />}
          title={'Font'}
          component={<FontSelector />}
        />
        <SideMenuItem
          setSignals={setSignals}
          ownSetter={setFontSizeOpen}
          ownGetter={fontSizeOpen()}
          icon={<TbTextResize size={36} />}
          title={'Font Size'}
          component={<FontSizeSelector />}
        />
        <SideMenuItem
          setSignals={setSignals}
          ownSetter={setLineHeightOpen}
          ownGetter={lineHeightOpen()}
          icon={<TbLineHeight size={36} />}
          title={'Line Height'}
          component={<LineHeightSelector />}
        />
        <SideMenuItem
          setSignals={setSignals}
          ownSetter={setChaptersOpen}
          ownGetter={chaptersOpen()}
          icon={<TbListNumbers size={36} />}
          title={'Chapters'}
          component={<MenuChapterList />}
        />
      </Show>
      <SideMenuItem
        setSignals={setSignals}
        ownSetter={setSearchOpen}
        ownGetter={searchOpen()}
        icon={<TbSearch size={36} />}
        title={'Search'}
        component={<SearchAllBooks />}
      />
    </div>
  )
}
