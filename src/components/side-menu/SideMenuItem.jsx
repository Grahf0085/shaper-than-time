import { TbChevronUp, TbChevronDown } from 'solid-icons/tb'
import { TransitionComponent } from './TransitionComponent.jsx'

export const SideMenuItem = (props) => {
  return (
    <>
      <button
        type='button'
        onClick={() => {
          if (window.matchMedia('(max-width: 768px)').matches) {
            props.setSignals.forEach((signal) => {
              if (signal !== props.ownSetter) signal(false)
            })
          }
          props.ownSetter(!props.ownGetter)
        }}
        class='flex items-center justify-between rounded-sm p-4 hover:bg-subMenuColor md:w-full'
      >
        <figure class='flex'>
          {props.icon}
          <figcaption class='flex items-center max-md:hidden'>
            {props.title}
          </figcaption>
        </figure>
        <TransitionComponent
          component={<TbChevronUp size={30} />}
          fallback={<TbChevronDown size={30} />}
          showWhen={props.ownGetter}
          enterDuration={500}
          exitDuration={0}
          wFull={false}
        />
      </button>
      <TransitionComponent
        component={props.component}
        showWhen={props.ownGetter}
        enterDuration={500}
        exitDuration={500}
        wFull={true}
      />
    </>
  )
}
