import { Show } from 'solid-js'
import { useNavigate, useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { TbBookmarks } from 'solid-icons/tb'
import { createParagraphRefs } from '~/providers/RefProvider'
import { getBookMark } from '~/server/reader'
import { getReader } from '~/server/session'

export const GoToBookmark = () => {
  const navigate = useNavigate()
  const params = useParams()

  const paragraphRefs = createParagraphRefs()

  const reader = createServerData$((_, { request }) => getReader(request))

  const bookmark = createServerData$((value) => getBookMark(value), {
    key() {
      return reader()?.id
    },
  })

  const handleBookmark = () => {
    if (
      decodeURI(params.title) !== bookmark()?.bookTitle ||
      decodeURI(params.translator) !== bookmark()?.translatorName
    ) {
      navigate(`/book/${bookmark()?.bookTitle}/${bookmark()?.translatorName}`)

      const checkIfDoneRouting = setInterval(function () {
        const decodedTitle = decodeURI(params.title)
        const decodedTranslator = decodeURI(params.translator)
        const bookmarkBookTitle = bookmark()?.bookTitle
        const bookmarkTranslatorName = bookmark()?.translatorName

        if (
          decodedTitle === bookmarkBookTitle &&
          decodedTranslator === bookmarkTranslatorName
        ) {
          const matchingRef = () =>
            paragraphRefs().find((ref) => ref.id() === bookmark()?.paragraphId)

          matchingRef()?.ref.scrollIntoView({
            behavior: 'smooth',
          })

          clearInterval(checkIfDoneRouting)
        }
      }, 1000)
    } else {
      const matchingRef = () =>
        paragraphRefs().find((ref) => ref.id() === bookmark()?.paragraphId)

      matchingRef()?.ref.scrollIntoView({
        behavior: 'smooth',
      })
    }
  }

  return (
    <button
      type='button'
      onClick={() => handleBookmark()}
      class='rounded-sm p-4 hover:bg-subMenuColor lg:w-full'
      disabled={!bookmark()}
    >
      <figure class='flex items-center'>
        <TbBookmarks size={36} />
        <figcaption class='text-xs max-md:hidden'>
          <Show when={bookmark()} fallback={<>No Bookmark</>}>
            {bookmark()?.bookTitle}({bookmark()?.translatorName}),{' '}
            {bookmark()?.chapterName}, {bookmark()?.paragraphNumber}
          </Show>
        </figcaption>
      </figure>
    </button>
  )
}
