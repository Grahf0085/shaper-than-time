import { Show } from 'solid-js'
import { Transition } from 'solid-transition-group'

export const TransitionComponent = (props) => {
  return (
    <Transition
      onEnter={(el, done) => {
        const a = el.animate([{ opacity: 0 }, { opacity: 1 }], {
          duration: props.enterDuration,
        })
        a.finished.then(done)
      }}
      onExit={(el, done) => {
        const a = el.animate([{ opacity: 1 }, { opacity: 0 }], {
          duration: props.exitDuration,
        })
        a.finished.then(done)
      }}
    >
      <Show when={props.showWhen === true} fallback={props.fallback}>
        <ul
          class={`max-md:flex max-md:overflow-scroll justify-between text-sm ${
            props.wFull === true
              ? 'w-full max-md:fixed max-md:right-0 max-md:top-40 max-md:bg-subMenuColor'
              : 'max-md:transparent w-auto'
          }`}
        >
          {props.component}
        </ul>
      </Show>
    </Transition>
  )
}
