import { TbBrandGitlab, TbCopyleft } from 'solid-icons/tb'

export const Footer = () => {
  return (
    <footer class='flex flex-wrap items-center rounded-sm bg-subMenuColor font-rubik text-sm max-md:flex-col-reverse md:justify-between'>
      <ul class='flex max-md:w-full md:contents'>
        <li class='px-4 max-md:w-1/2 max-md:py-2 md:py-4'>
          <a
            href='https://gitlab.com/Grahf0085/sharper-than-time'
            target='_blank'
            class='flex items-center justify-center'
          >
            Front <TbBrandGitlab size={24} />
          </a>
        </li>
        <li class='px-4 max-md:w-1/2 max-md:py-2 md:py-4'>
          <a
            href='https://gitlab.com/Grahf0085/make-nietzsche-db'
            target='_blank'
            class='flex items-center justify-center'
          >
            Back(san judges) <TbBrandGitlab size={24} />
          </a>
        </li>
      </ul>
      <a
        href='mailto:sharperthantime@tutanota.com'
        class=' px-4 text-center max-md:w-full max-md:py-2 md:py-4'
      >
        sharperthantime@tutanota.com
      </a>
      <p class='flex items-center px-4 max-md:py-2 md:py-4'>
        Copyleft <TbCopyleft size={24} /> 2023 Sharper Than Time
      </p>
    </footer>
  )
}
