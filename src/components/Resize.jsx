import { createEffect, createSignal, on, onCleanup, onMount } from 'solid-js'
import { debounce } from '@solid-primitives/scheduled'
import {
  createSelectedFont,
  createSelectedFontSize,
  createSelectedLineHeight,
} from '~/providers/FontProvider'
import { createTextOnScreen, createDrawerOpen } from '~/providers/OtherProvider'
import {
  createBookRefs,
  createFullTextRef,
  createScrollWidth,
  createSetScrollWidth,
} from '~/providers/RefProvider'
import { createSearchResults } from '~/providers/SearchProvider'

export const Resize = (props) => {
  let fullTextResizeObserver

  const [scrollEvent] = createSignal('scroll')
  const [transitionRunEvent] = createSignal('transitionrun')
  const [transitionEndEvent] = createSignal('transitionend')
  const [resizeEvent] = createSignal('resize')

  const fullTextRef = createFullTextRef()
  const bookRefs = createBookRefs()
  const scrollWidth = createScrollWidth()
  const setScrollWidth = createSetScrollWidth()
  const font = createSelectedFont()
  const fontSize = createSelectedFontSize()
  const lineHeight = createSelectedLineHeight()
  const textOnScreen = createTextOnScreen()
  const searchResults = createSearchResults()
  const drawerOpen = createDrawerOpen()

  const handleScrollTo = (toScrollTo) => {
    if (toScrollTo) toScrollTo.scrollIntoView({ behavior: 'smooth' })
  }

  const debouncedHandleScrollTo = debounce(handleScrollTo, 500)

  createEffect(() => {
    const event = scrollEvent()
    const callback = () => {
      const percentScrolled =
        fullTextRef().scrollLeft / (scrollWidth() - props.clientWidth)
      const newPage = Math.round(percentScrolled * props.maxPage)
      props.setCurrentPage(newPage)
    }
    fullTextRef().addEventListener(event, callback)
    onCleanup(() => fullTextRef().removeEventListener(event, callback))
  })

  createEffect(() => {
    const event = transitionRunEvent()
    const callback = () => {
      bookRefs().forEach((reference) =>
        props.intersectionObserver.unobserve(reference),
      )
    }
    fullTextRef().addEventListener(event, callback)
    onCleanup(() => fullTextRef().removeEventListener(event, callback))
  })

  createEffect(() => {
    const event = transitionEndEvent()
    const callback = () => {
      props.setClientWidth(fullTextRef().clientWidth)
      setScrollWidth(fullTextRef().scrollWidth)
      bookRefs().forEach((reference) =>
        props.intersectionObserver.observe(reference),
      )
    }
    fullTextRef().addEventListener(event, callback)
    onCleanup(() => fullTextRef().removeEventListener(event, callback))
  })

  createEffect(() => {
    const event = resizeEvent()
    const callback = () => {
      props.setClientWidth(fullTextRef().clientWidth)
      setScrollWidth(fullTextRef().scrollWidth)
      debouncedHandleScrollTo(textOnScreen())
    }
    window.addEventListener(event, callback)
    onCleanup(() => window.removeEventListener(event, callback))
  })

  const onlyResultsOfSearch = () => {
    if (window.matchMedia('(min-width: 1024px)').matches)
      return searchResults()?.matches
  }

  createEffect(
    on(
      [onlyResultsOfSearch, font, fontSize, lineHeight, drawerOpen],
      () => {
        setScrollWidth(fullTextRef().scrollWidth)
        debouncedHandleScrollTo(textOnScreen())
      },
      { defer: false },
    ),
  )

  onMount(() => {
    //if only height is changed doesn't always scroll to right element
    //there is a createEffect to watch for resize of window to handle what to scroll to and this resize observer to properly set variables for page and max page
    fullTextResizeObserver = new ResizeObserver((entries) => {
      for (const entry of entries) {
        props.setClientWidth(fullTextRef().clientWidth)
        setScrollWidth(fullTextRef().scrollWidth)
      }
    })

    fullTextResizeObserver.observe(fullTextRef())
  })

  onCleanup(() => {
    if (fullTextResizeObserver) fullTextResizeObserver.disconnect()
  })
}
