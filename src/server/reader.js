import bcrypt from 'bcryptjs'
import pool from './pool.js'
import dotenv from 'dotenv'

dotenv.config()

export async function createReader({ data }) {
  const result = await pool.query(
    `
INSERT INTO readers
VALUES (DEFAULT, $1, $2)
RETURNING *
`,
    [
      data.username,
      bcrypt
        .hashSync(
          data.password + process.env.PEPPER,
          Number(process.env.SALT_ROUNDS),
        )
        .toString(),
    ],
  )

  await pool.query(
    `
INSERT INTO bookmarks
VALUES (DEFAULT, $1, NULL)
`,
    [result.rows[0].id],
  )
  return result.rows[0]
}

export async function findReader({
  where: { username = undefined, id = undefined },
}) {
  if (id !== undefined && !isNaN(id)) {
    const result = await pool.query('SELECT * FROM readers WHERE id = $1', [id])
    return result.rows[0]
  } else if (username !== undefined) {
    const result = await pool.query(
      'SELECT * FROM readers WHERE username = $1',
      [username],
    )
    return result.rows[0]
  }
  return null
}

export async function createBookmark(readerId, paragraphId) {
  const { rows } = await pool.query(
    `
UPDATE bookmarks
SET paragraph_fk = $2
WHERE reader_fk = $1
RETURNING *
    `,
    [readerId, paragraphId],
  )
  return rows
}

export async function getBookMark(readerId) {
  const { rows } = await pool.query(
    `
SELECT translator_name AS "translatorName", book_title AS "bookTitle", chapter_name AS "chapterName", paragraph_number AS "paragraphNumber", paragraphs.id AS "paragraphId" FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE paragraphs.id = (SELECT paragraph_fk FROM bookmarks WHERE reader_fk = $1)
`,
    [readerId],
  )
  return rows[0]
}

export async function createNotes(readerId, paragraphId, notes) {
  const { rows } = await pool.query(
    `
INSERT INTO notes
VALUES(DEFAULT, $1, $2, DEFAULT, $3)
RETURNING *
`,
    [readerId, paragraphId, notes],
  )
  return rows
}

export async function updateNotes(readerId, paragraphId, notes) {
  const { rows } = await pool.query(
    `
UPDATE notes
SET notes = $3, last_edited = CURRENT_DATE
WHERE reader_fk = $1 AND paragraph_fk = $2
RETURNING *
`,
    [readerId, paragraphId, notes],
  )
  return rows
}

export async function getNotes(readerId, paragraphId) {
  const { rows } = await pool.query(
    `
SELECT notes FROM notes
WHERE reader_fk = $1 AND paragraph_fk = $2
`,
    [readerId, paragraphId],
  )
  if (rows.length > 0) {
    return rows[0].notes
  } else return ''
}

export async function getAllNotesForReader(readerId) {
  const { rows } = await pool.query(
    `
SELECT paragraph_number AS "paragraphNumber", chapter_name AS "chapterName", book_title AS "bookTitle", translator_name AS "translatorName", notes, last_edited AS "lastEdited"
FROM notes
INNER JOIN paragraphs ON paragraphs.id = notes.paragraph_fk 
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE notes.reader_fk = $1
`,
    [readerId],
  )
  return rows
}
