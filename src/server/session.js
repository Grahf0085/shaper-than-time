import bcrypt from 'bcryptjs'
import dotenv from 'dotenv'
import { createCookieSessionStorage } from 'solid-start'
import { createReader, findReader } from './reader'
import { redirect } from 'solid-start/server'

dotenv.config()

export async function register({ username, password }) {
  return createReader({ data: { username, password } })
}

export async function login({ username, password }) {
  const reader = await findReader({ where: { username } })
  if (!reader) return null
  let isCorrectPassword = bcrypt.compareSync(
    password + process.env.PEPPER,
    reader.digested_password,
  )
  if (!isCorrectPassword) return null
  return reader
}

const storage = createCookieSessionStorage({
  cookie: {
    name: 'reader_session',
    secure: true,
    secrets: [process.env.SESSION_SECRET],
    sameSite: 'lax',
    path: '/',
    maxAge: 60 * 60 * 24 * 30, // 30days
    httpOnly: true,
  },
})

export function getReaderSession(request) {
  return storage.getSession(request.headers.get('Cookie'))
}

export async function getReaderId(request) {
  const session = await getReaderSession(request)
  const readerId = session.get('readerId')
  if (!readerId || typeof readerId !== 'string') return null
  return readerId
}

export async function getReader(request) {
  const readerId = await getReaderId(request)
  if (typeof readerId !== 'string') {
    return null
  }
  try {
    const user = await findReader({ where: { id: Number(readerId) } })
    return user
  } catch {
    throw logout(request)
  }
}

export async function logout(request, redirectTo) {
  const session = await storage.getSession(request.headers.get('Cookie'))
  return redirect(redirectTo, {
    headers: {
      'Set-Cookie': await storage.destroySession(session),
    },
  })
}

export async function createReaderSession(readerId, redirectTo) {
  const session = await storage.getSession()
  session.set('readerId', readerId)
  return redirect(redirectTo, {
    headers: {
      'Set-Cookie': await storage.commitSession(session),
    },
  })
}
