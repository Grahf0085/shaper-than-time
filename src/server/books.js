import pool from './pool.js'

export async function getQuote() {
  const { rows } = await pool.query(
    'SELECT quote_text AS "quoteText" FROM quotes',
  )
  const randomNumber = Math.floor(Math.random() * rows.length)
  return {
    quoteText: rows[randomNumber].quoteText,
    commentary: rows[randomNumber].chatgptCommentary,
  }
}

export async function getAllTitles() {
  const { rows } = await pool.query('SELECT book_title AS book FROM books')
  const allTitles = rows.map((row) => row.book)
  return [...new Set(allTitles)]
}

export async function getTranslations(title) {
  const { rows } = await pool.query(
    `
SELECT translator_name AS translator
FROM translator
INNER JOIN books
ON books.translator_fk = translator.id
WHERE books.book_title = $1
`,
    [decodeURI(title)],
  )
  return rows.map((row) => row.translator)
}

export async function getBookInfo(title, translator) {
  const { rows } = await pool.query(
    `
SELECT book_title AS "bookTitle", sub_title AS "subTitle", pub_date AS "pubDate", translated_date AS "translatedDate", translator_name AS "translatorName"
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
WHERE book_title = $1 AND translator_name = $2
`,
    [decodeURI(title), decodeURI(translator)],
  )
  return rows
}

export async function getChapterList(title, translator) {
  const { rows } = await pool.query(
    `
SELECT chapter_number AS "chapterNumber", chapter_name AS "chapterName"
FROM chapters
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE book_title = $1 AND translator_name = $2
`,
    [decodeURI(title), decodeURI(translator)],
  )
  return rows
}

export async function getChapterParagraphs(title, translator, chapter) {
  const { rows } = await pool.query(
    `
SELECT chapter_number AS "chapterNumber", paragraph_number AS "paragraphNumber", paragraph_text AS "paragraphText", paragraphs.id AS "paragraphId"
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
WHERE book_title = $1 AND translator_name = $2 AND chapter_number = $3
ORDER BY SUBSTRING(paragraph_number FROM '([0-9]+)')::BIGINT ASC, paragraph_number

`,
    [decodeURI(title), decodeURI(translator), chapter],
  )
  return rows
}

export async function getOtherTranslationOfParagraph(
  title,
  translator,
  chapter,
) {
  const { rows } = await pool.query(
    `
SELECT chapter_number AS "chapterNumber", paragraph_number AS "paragraphNumber", paragraph_text AS "paragraphText", paragraphs.id AS "paragraphId", translator_name AS "translatorName"
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
WHERE book_title = $1 AND translator_name != $2 AND chapter_number = $3
ORDER BY paragraphs.id
`,
    [decodeURI(title), decodeURI(translator), chapter],
  )
  return rows
}

export async function getFootnotes(title, translator, chapter, paragraph) {
  const { rows } = await pool.query(
    `
SELECT footnote_text AS footnote
FROM books
INNER JOIN translator ON translator.id = books.translator_fk
INNER JOIN chapters ON chapters.book_fk = books.id
INNER JOIN paragraphs ON paragraphs.chapter_fk = chapters.id
INNER JOIN footnotes ON footnotes.paragraph_fk = paragraphs.id 
WHERE book_title = $1 AND translator_name = $2 AND chapter_number = $3 AND paragraph_number = $4
ORDER BY footnotes.id
`,
    [decodeURI(title), decodeURI(translator), chapter, paragraph],
  )
  return rows.map((row) => row.footnote)
}

export async function getSeeAlso(paragraphFK) {
  const { rows } = await pool.query(
    `
SELECT paragraph_text AS "paragraphText", paragraph_number AS "paragraphNumber", chapter_name AS "chapterName", chapter_number AS "chapterNumber", book_title AS "bookTitle", translator_name AS "translatorName"
FROM see_also
INNER JOIN paragraphs ON paragraphs.id = paragraph_two_fk
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE paragraph_one_fk = $1
`,
    [paragraphFK],
  )
  return rows
}

export async function getSingleBookSearch(term, title, translator) {
  const { rows } = await pool.query(
    `
SELECT substring(paragraph_text,
    greatest(1, position(LOWER($1) IN LOWER(paragraph_text)) - 100),
    least(200, length(paragraph_text) - position(LOWER($1) IN LOWER(paragraph_text)) + 100)) AS "paragraphText", paragraph_number AS "paragraphNumber", 
    chapter_number AS "chapterNumber", paragraphs.id AS "paragraphId", chapter_name AS "chapterName",  
     paragraphs.id AS "paragraphId", ts_rank(paragraph_tokens, websearch_to_tsquery($1)) as rank
FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE book_title = $2 AND translator_name = $3 AND ts_rank(paragraph_tokens, websearch_to_tsquery($1)) > 0
ORDER BY rank DESC
`,
    [term, decodeURI(title), decodeURI(translator)],
  )
  return rows
}

export async function getMultipleBookSearch(term, books) {
  const { rows } = await pool.query(
    `
SELECT substring(paragraph_text,
    greatest(1, position(LOWER($1) IN LOWER(paragraph_text)) - 100),
    least(200, length(paragraph_text) - position(LOWER($1) IN LOWER(paragraph_text)) + 100)) AS "paragraphText", paragraph_number AS "paragraphNumber", 
    paragraphs.id AS "paragraphId", chapter_number AS "chapterNumber", chapter_name AS "chapterName", book_title AS "bookTitle", translator_name AS "translatorName", 
    ts_rank(paragraph_tokens, websearch_to_tsquery($1)) as rank
FROM paragraphs
INNER JOIN chapters ON chapters.id = paragraphs.chapter_fk
INNER JOIN books ON books.id = chapters.book_fk
INNER JOIN translator ON translator.id = books.translator_fk
WHERE ts_rank(paragraph_tokens, websearch_to_tsquery($1)) > 0 AND book_title = ANY($2::text[])
ORDER BY rank DESC
`,
    [term, books],
  )
  return rows
}
