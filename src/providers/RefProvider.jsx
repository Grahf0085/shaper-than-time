import { createSignal, createContext, useContext } from 'solid-js'

export const RefContext = createContext()

export const RefProvider = (props) => {
  const [refForFullText, setRefForFullText] = createSignal()
  const [scrollWidth, setScrollWidth] = createSignal(
    () => refForFullText().scrollWidth,
  )
  const [refForIntersection, setRefForIntersection] = createSignal([])
  const [refForParagraph, setRefForParagraph] = createSignal([])
  const [refForSideBar, setRefForSideBar] = createSignal()

  return (
    <RefContext.Provider
      value={[
        refForFullText,
        setRefForFullText,
        scrollWidth,
        setScrollWidth,
        refForIntersection,
        setRefForIntersection,
        refForParagraph,
        setRefForParagraph,
        refForSideBar,
        setRefForSideBar,
      ]}
    >
      {props.children}
    </RefContext.Provider>
  )
}

export const createFullTextRef = () => {
  const newFullTextRef = useContext(RefContext)[0]
  return newFullTextRef
}

export const createSetFullTextRef = () => {
  const setNewFullTextRef = useContext(RefContext)[1]
  return setNewFullTextRef
}

//scrollWidth in this provider becuase it depends on property of fullTextRef
export const createScrollWidth = () => {
  const newScrollWidth = useContext(RefContext)[2]
  return newScrollWidth
}

export const createSetScrollWidth = () => {
  const setNewScrollWidth = useContext(RefContext)[3]
  return setNewScrollWidth
}

export const createBookRefs = () => {
  const newBookRefs = useContext(RefContext)[4]
  return newBookRefs
}

export const createSetBookRefs = () => {
  const setNewBookRefs = useContext(RefContext)[5]
  return setNewBookRefs
}

export const createParagraphRefs = () => {
  const newParagraphRefs = useContext(RefContext)[6]
  return newParagraphRefs
}

export const createSetParagraphRefs = () => {
  const setNewParagraphRefs = useContext(RefContext)[7]
  return setNewParagraphRefs
}

export const createSideBarRef = () => {
  const newSideBarRef = useContext(RefContext)[8]
  return newSideBarRef
}

export const createSetSideBarRef = () => {
  const setNewSideBarRef = useContext(RefContext)[9]
  return setNewSideBarRef
}
