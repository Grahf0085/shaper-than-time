import { createContext, createSignal, useContext } from 'solid-js'

export const SearchContext = createContext()

export const SearchProvider = (props) => {
  const [search, setSearch] = createSignal({
    term: '',
    matches: [],
  })
  const [selectedTitles, setSelectedTitles] = createSignal([])

  return (
    <SearchContext.Provider
      value={[search, setSearch, selectedTitles, setSelectedTitles]}
    >
      {props.children}
    </SearchContext.Provider>
  )
}

export const createSearchResults = () => {
  const newSearch = useContext(SearchContext)[0]
  return newSearch
}

export const createSetSearchResults = () => {
  const setNewSearch = useContext(SearchContext)[1]
  return setNewSearch
}

export const createSelectedTitles = () => {
  const newSelectedTitles = useContext(SearchContext)[2]
  return newSelectedTitles
}

export const createSetSelectedTitles = () => {
  const setNewSelectedTitles = useContext(SearchContext)[3]
  return setNewSelectedTitles
}
