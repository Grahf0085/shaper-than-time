import { createSignal, createContext, useContext } from 'solid-js'

export const OtherContext = createContext()

export const OtherProvider = (props) => {
  const [drawerOpen, setDrawerOpen] = createSignal(false)
  const [textOnScreen, setTextOnScreen] = createSignal()

  return (
    <OtherContext.Provider
      value={[drawerOpen, setDrawerOpen, textOnScreen, setTextOnScreen]}
    >
      {props.children}
    </OtherContext.Provider>
  )
}

export const createDrawerOpen = () => {
  const newDrawerOpen = useContext(OtherContext)[0]
  return newDrawerOpen
}

export const createSetDrawerOpen = () => {
  const setNewDrawerOpen = useContext(OtherContext)[1]
  return setNewDrawerOpen
}

export const createTextOnScreen = () => {
  const newTextOnScreen = useContext(OtherContext)[2]
  return newTextOnScreen
}

export const createSetTextOnScreen = () => {
  const setNewTextOnScreen = useContext(OtherContext)[3]
  return setNewTextOnScreen
}
