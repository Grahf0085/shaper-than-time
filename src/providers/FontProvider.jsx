import { createSignal, createContext, useContext } from 'solid-js'

export const FontContext = createContext()

export const FontProvider = (props) => {
  const [selectedFont, setSelectedFont] = createSignal('Inter')
  const [lineHeight, setLineHeight] = createSignal(1.5)
  const [fontSize, setFontSize] = createSignal('1rem')

  return (
    <FontContext.Provider
      value={[
        selectedFont,
        setSelectedFont,
        lineHeight,
        setLineHeight,
        fontSize,
        setFontSize,
      ]}
    >
      {props.children}
    </FontContext.Provider>
  )
}

export const createSelectedFont = () => {
  const newSelectedFont = useContext(FontContext)[0]
  return newSelectedFont
}

export const createSetSelectedFont = () => {
  const setNewSelectedFont = useContext(FontContext)[1]
  return setNewSelectedFont
}

export const createSelectedLineHeight = () => {
  const newSelectedLineHeight = useContext(FontContext)[2]
  return newSelectedLineHeight
}

export const createSetSelectedLineHeight = () => {
  const setNewSelectedLineHeight = useContext(FontContext)[3]
  return setNewSelectedLineHeight
}

export const createSelectedFontSize = () => {
  const newSelectedFontSize = useContext(FontContext)[4]
  return newSelectedFontSize
}

export const createSetSelectedFontSize = () => {
  const setNewSelectedFontSize = useContext(FontContext)[5]
  return setNewSelectedFontSize
}
