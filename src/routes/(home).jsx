import { Suspense } from 'solid-js'
import { Outlet } from 'solid-start'
import { Header } from '~/components/Header'
import { SearchResults } from '~/components/SearchResults'
import { SideMenu } from '~/components/side-menu/SideMenu'
import { createDrawerOpen } from '~/providers/OtherProvider'
import { createSearchResults } from '~/providers/SearchProvider'
import {
  createSelectedFont,
  createSelectedFontSize,
  createSelectedLineHeight,
} from '~/providers/FontProvider'

export default function Home() {
  const drawerOpen = createDrawerOpen()
  const searchResults = createSearchResults()
  const font = createSelectedFont()
  const lineHeight = createSelectedLineHeight()
  const fontSize = createSelectedFontSize()

  //TODO switch out 7rem with a variable - 5 from header height and 2 from margin
  return (
    <div class='flex h-screen w-screen flex-col bg-backgroundColor text-textColor'>
      <Header />
      <div class='m-4 h-[calc(100%-7rem)] text-textColor max-md:flex max-md:flex-col md:grid md:grid-cols-[auto_1fr_auto]'>
        <div
          class={`md:overflow-y-scroll duration-500 ${
            drawerOpen() === true
              ? 'w-full max-md:h-16 md:mr-4 md:w-56'
              : 'max-md:h-0 max-md:-translate-y-40 md:w-0'
          }`}
        >
          <SideMenu />
        </div>
        <div
          style={{
            'font-size': fontSize(),
            'line-height': lineHeight(),
            'font-family': font(),
          }}
          class={`flex flex-col md:justify-center min-h-0 h-full ${
            drawerOpen() === true || searchResults()?.matches?.length > 0
              ? 'md:max-w-[90%] md:w-auto md:justify-self-center'
              : 'md:max-w-[90%] md:w-full md:justify-self-center'
          } duration-500`}
        >
          <Outlet />
        </div>
        <div
          style={{
            'font-size': fontSize(),
            'line-height': lineHeight(),
            'font-family': font(),
          }}
          class={`overflow-y-scroll duration-500 max-lg:fixed max-lg:h-full flex flex-col items-end ${
            searchResults()?.matches?.length > 0
              ? 'w-full max-lg:right-0 max-lg:bg-menuColor md:ml-4 lg:w-[34rem]'
              : 'w-0 opacity-0 max-lg:right-[-100%]'
          }`}
        >
          <Suspense>
            <SearchResults />
          </Suspense>
        </div>
      </div>
    </div>
  )
}
