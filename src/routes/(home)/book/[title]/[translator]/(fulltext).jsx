import {
  createSignal,
  createEffect,
  onCleanup,
  onMount,
  Suspense,
  Show,
} from 'solid-js'
import { Portal } from 'solid-js/web'
import { useNavigate, useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { Intersection } from '~/components/Intersection'
import { Resize } from '~/components/Resize'
import { Slider } from '~/components/Slider'
import { BookInfo } from '~/components/book-parts/BookInfo'
import { ChapterList } from '~/components/book-parts/ChapterList'
import { Chapters } from '~/components/book-parts/Chapters'
import {
  createFullTextRef,
  createSetFullTextRef,
  createScrollWidth,
  createSetScrollWidth,
  createSideBarRef,
} from '~/providers/RefProvider'
import { createSearchResults } from '~/providers/SearchProvider'
import { getAllTitles, getTranslations } from '~/server/books'

export default function Fulltext() {
  const [clientWidth, setClientWidth] = createSignal()
  const [currentPage, setCurrentPage] = createSignal(0)
  const [chapterRefs, setChapterRefs] = createSignal([])
  const [currentChapter, setCurrentChapter] = createSignal()
  const [intersectionObserver, setIntersectionObserver] = createSignal()
  const [keyDownEvent] = createSignal('keydown')

  const params = useParams()
  const navigate = useNavigate()

  const fullTextRef = createFullTextRef()
  const scrollWidth = createScrollWidth()
  const setScrollWidth = createSetScrollWidth()
  const setFullTextRef = createSetFullTextRef()
  const sideBarRef = createSideBarRef()
  const searchResults = createSearchResults()

  const maxPage = () => Math.ceil(scrollWidth() / clientWidth() - 1)

  const titles = createServerData$(() => getAllTitles())

  const translators = createServerData$((value) => getTranslations(value), {
    key() {
      return params.title
    },
  })

  //this will need to be replaced with something like this: https://start.solidjs.com/api/HttpStatusCode#setting-a-404-status-code-for-the-unmatched-routes
  onMount(() => {
    //stop people from using random titles and translators
    if (
      !titles().includes(decodeURI(params.title)) ||
      !translators().includes(decodeURI(params.translator))
    )
      navigate('/not-found')
  })

  createEffect(() => {
    //prevent search with cntrl + f
    const event = keyDownEvent()
    const callback = (event) => {
      if (event.key === 'F3' || (event.ctrlKey && event.key === 'f'))
        event.preventDefault()
    }
    fullTextRef().addEventListener(event, callback)
    onCleanup(() => fullTextRef().removeEventListener(event, callback))
  })

  return (
    <>
      <Intersection
        setCurrentChapter={setCurrentChapter}
        setIntersectionObserver={setIntersectionObserver}
      />
      <Resize
        clientWidth={clientWidth()}
        setClientWidth={setClientWidth}
        maxPage={maxPage()}
        setCurrentPage={setCurrentPage}
        intersectionObserver={intersectionObserver()}
      />
      <div
        ref={(el) => {
          setFullTextRef(el)
          createEffect((prev) => {
            const selectedBook = params.title + params.translator
            if (selectedBook !== prev) {
              setFullTextRef(el)
              setScrollWidth(fullTextRef().scrollWidth)
              //if searchResults are open then the user is clicking on a search result and we don't want to go to the beginning of the book - we want to go where the search result is
              if (searchResults()?.matches?.length === 0) {
                setCurrentPage(0)
                fullTextRef().scrollLeft = 0
                fullTextRef().focus()
              }
            }
            return selectedBook
          })
        }}
        tabIndex={-1}
        class='no-scrollbar flex snap-x snap-mandatory flex-col flex-wrap overflow-y-hidden'
      >
        <BookInfo />
        <ChapterList sideMenu={false} chapterRefs={chapterRefs()} />
        <Chapters setChapterRefs={setChapterRefs} />
      </div>
      <Slider
        currentPage={currentPage()}
        setCurrentPage={setCurrentPage}
        maxPage={maxPage()}
        currentChapter={currentChapter()}
      />
      <Show when={sideBarRef()}>
        <Portal mount={sideBarRef()}>
          <Suspense>
            <ChapterList
              chapterRefs={chapterRefs()}
              currentChapter={currentChapter()}
              sideMenu={true}
            />
          </Suspense>
        </Portal>
      </Show>
    </>
  )
}
