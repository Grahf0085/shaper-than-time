import { createSignal, For, onMount, Show, Suspense } from 'solid-js'
import { useNavigate, useParams } from 'solid-start'
import { createServerData$ } from 'solid-start/server'
import { getAllNotesForReader } from '~/server/reader'
import { getReader } from '~/server/session'

export default function ReaderNotes() {
  const [correctPath, setCorrectPath] = createSignal(false)

  const params = useParams()
  const navigate = useNavigate()

  const reader = createServerData$((_, { request }) => getReader(request))

  const notes = createServerData$(([id]) => getAllNotesForReader(id), {
    key: () => [reader()?.id],
  })

  onMount(() => {
    //stop people from using random user names in params
    //this will need to be replaced with something like this: https://start.solidjs.com/api/HttpStatusCode#setting-a-404-status-code-for-the-unmatched-routes
    if (reader()?.username === params.reader) {
      setCorrectPath(true)
    } else {
      navigate('/not-found')
    }
  })

  return (
    <Show when={correctPath()}>
      <Suspense>
        <div class='hidden h-full self-center overflow-y-scroll font-rubik md:block'>
          <table class='table-auto'>
            <thead>
              <tr>
                <th class='border-b border-ternaryColor bg-menuColor px-8 py-4 text-left font-bold'>
                  Book
                </th>
                <th class='border-b border-ternaryColor bg-menuColor px-8 py-4 text-left font-bold'>
                  Chapter
                </th>
                <th class='border-b border-ternaryColor bg-menuColor px-8 py-4 text-left font-bold'>
                  Paragraph
                </th>
                <th class='border-b border-ternaryColor bg-menuColor px-8 py-4 text-left font-bold'>
                  Notes
                </th>
                <th class='border-b border-ternaryColor bg-menuColor px-8 py-4 text-left font-bold'>
                  Last Edited
                </th>
              </tr>
            </thead>
            <tbody>
              <For each={notes()} fallback={<></>}>
                {(note) => {
                  const lastEditedDate = new Date(note.lastEdited)
                  return (
                    <tr class='odd:bg-subMenuColor'>
                      <td class='border-b border-ternaryColor px-8 py-4 text-left'>
                        {note.bookTitle} ({note.translatorName})
                      </td>
                      <td class='border-b border-ternaryColor px-8 py-4 text-left'>
                        {note.chapterName}
                      </td>
                      <td class='border-b border-ternaryColor px-8 py-4 text-left'>
                        {note.paragraphNumber}
                      </td>
                      <td class='border-b border-ternaryColor px-8 py-4 text-left'>
                        {note.notes}
                      </td>
                      <td class='border-b border-ternaryColor px-8 py-4 text-left'>
                        <time datetime={lastEditedDate.toDateString()}>
                          {lastEditedDate.toDateString()}
                        </time>
                      </td>
                    </tr>
                  )
                }}
              </For>
            </tbody>
          </table>
        </div>

        <div class='h-full w-full overflow-y-scroll font-rubik md:hidden'>
          <For
            each={notes()}
            fallback={
              <div class='flex h-full w-full items-center justify-center'>
                No Thoughts Found
              </div>
            }
          >
            {(note) => {
              const lastEditedDate = new Date(note.lastEdited)
              return (
                <div class='m-4 flex flex-col gap-4 rounded-sm bg-subMenuColor p-4'>
                  <div class='border-b-2 border-ternaryColor pb-4 text-center'>
                    <cite>{note.bookTitle}</cite>({note.translatorName}),
                    {note.chapterName}, Paragraph {note.paragraphNumber}
                  </div>
                  <div>{note.notes}</div>
                  <div class='border-t-2 border-ternaryColor pt-4 text-center'>
                    {lastEditedDate.toDateString()}
                  </div>
                </div>
              )
            }}
          </For>
        </div>
      </Suspense>
    </Show>
  )
}
