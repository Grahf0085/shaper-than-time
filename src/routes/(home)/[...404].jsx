import { HttpStatusCode } from 'solid-start/server'

export default function NotFound() {
  return (
    <div class='flex h-full w-full flex-col items-center justify-around bg-menuColor font-rubik text-textColor'>
      <HttpStatusCode code={404} />
      <figure class='text-6xl lg:text-7xl'>
        <blockquote class='mb-20'>Not all who wonder are lost</blockquote>
        <figcaption class='text-right'>
          —J.R.R. Tolkein, <cite>The Fellowship of the Ring</cite>
        </figcaption>
      </figure>
      <p>...but maybe rethink your goal</p>
    </div>
  )
}
