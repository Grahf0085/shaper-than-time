import { createServerData$ } from 'solid-start/server'
import { Footer } from '~/components/Footer'
import { getQuote } from '~/server/books'

export default function Home() {
  const quote = createServerData$(() => getQuote())

  return (
    <>
      <figure class='flex h-full flex-col font-rubik text-xl max-md:overflow-y-scroll md:justify-center md:self-center md:overflow-y-scroll md:text-4xl md:w-full mb-4'>
        <blockquote class='pb-10 italic md:h-fit md:overflow-y-scroll md:w-full md:text-center'>
          {quote()?.quoteText}
        </blockquote>
        <figcaption class='text-right'>-N.</figcaption>
      </figure>
      <Footer />
    </>
  )
}
