import { redirect } from 'solid-start'
import {
  StartServer,
  createHandler,
  renderAsync,
} from 'solid-start/entry-server'
import { getReaderSession } from './server/session'

const protectedPaths = ['/reader']

function isProtected(route) {
  return protectedPaths.find((path) => (route.startsWith(path) ? true : false))
}

export default createHandler(
  ({ forward }) => {
    return async (event) => {
      const session = await getReaderSession(event.request)
      const readerId = session.get('readerId')
      const path = new URL(event.request.url).pathname

      if (isProtected(path) && !readerId) {
        return redirect('/not-found')
      }

      return forward(event)
    }
  },
  renderAsync((event) => <StartServer event={event} />),
)
