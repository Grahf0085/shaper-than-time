// @refresh reload
import { Suspense } from 'solid-js'
import { Dynamic } from 'solid-js/web'
import {
  Body,
  ErrorBoundary,
  FileRoutes,
  Head,
  Html,
  Link,
  Meta,
  Routes,
  Scripts,
  Title,
} from 'solid-start'
import './root.css'
import { OtherProvider } from './providers/OtherProvider'
import { RefProvider } from './providers/RefProvider'
import { FontProvider } from './providers/FontProvider'
import { SearchProvider } from './providers/SearchProvider'

export default function Root() {
  return (
    <Html lang='en'>
      <Head>
        <Title>Sharper Than Time</Title>
        <Meta charset='utf-8' />
        <Meta
          name='description'
          content='A website to read Nietzsches books. Shaper Than Time'
        />
        <Meta name='viewport' content='width=device-width, initial-scale=1' />
        <Link rel='preconnect' href='https://rsms.me/' />
        <Link rel='stylesheet' href='https://rsms.me/inter/inter.css' />
        <Dynamic component='script'>{`
if (sessionStorage.theme === 'light') 
  if(window.matchMedia('(prefers-color-scheme: dark)').matches)
    document.documentElement.classList.add('light')

if (sessionStorage.theme === 'dark') 
  if(window.matchMedia('(prefers-color-scheme: light)').matches)
    document.documentElement.classList.add('dark') 
`}</Dynamic>
      </Head>
      <Body>
        <Suspense>
          <ErrorBoundary>
            <SearchProvider>
              <FontProvider>
                <RefProvider>
                  <OtherProvider>
                    <Routes>
                      <FileRoutes />
                    </Routes>
                  </OtherProvider>
                </RefProvider>
              </FontProvider>
            </SearchProvider>
          </ErrorBoundary>
        </Suspense>
        <Scripts />
      </Body>
    </Html>
  )
}
